# Segundo chakra o chakra del ombligo
El chakra del ombligo se localiza en la zona del mismo nombre, está abierto hacia delante y tiene también un vórtice posterior; es el chakra de la propagación de la especie y, por lo tanto, de la reproducción. Como consecuencia lógica representa las relaciones afectivas, en lo que concierne al placer sexual. Es el chakra que concentra las cualidades que tienen que ver con la sexualidad, la curiosidad, la búsqueda creativa del placer material, el gusto por las cosas bellas, por el arte, por las emociones y, obviamente, las relaciones con otros individuos. Por ejemplo: el amor sexual, la apertura hacia cosas nuevas, las relaciones afectivas, amorosas y sexuales.

Es un chakra fundamental, cuya actividad correcta nos permite amar la vida, haciendo que sea más placentera. Si funciona mal, puede transformar la vida en un pequeño “infierno” personal que termina reflejándose en las personas con las que vivimos y nos relacionamos.

Este chakra es la sede de los miedos, de los fantasmas y fantasías negativas vinculadas a la sexualidad, y del comportamiento hacia el sexo opuesto.

Los bloqueos en el centro sexual devienen frecuentemente en síntomas mentales, como miedo de la proximidad física (“¡no me toques!”), y repugnancia por el cuerpo (“¡el sexo es para los animales; los seres humanos nacieron para algo más elevado!”), manía de limpieza, incomprensión (“¡no entiendo!”), una mente muy centrada en la razón (“¿para qué sirven los sentimientos?”), énfasis excesivo en sentimientos impulsivos (“¿para qué reflexionar?, ¡Yo actúo por instinto!”), desórdenes rítmicos (“¡ni sé, ni puedo bailar!”), (“¿por qué sufro siempre de dolores menstruales?”), (“¡prefiero trabajar de noche!”), aislamiento (“¡casamiento y relación no me sirven de nada!”), frigidez, impotencia, falta de apetito sexual (“¡no necesito sexo, no veo lo que los demás obtienen de ello!”), miedo de caerse (“¡nunca saltaría de un trampolín!”). La afirmación “no tiene alegría de vivir”, resume la condición de un chakra sexual desordenado; los bloqueos en ese chakra
acaban frecuentemente en síntomas físicos, como enfermedades relacionadas con los fluidos del cuerpo (laringe, linfa, saliva, bilis) o con órganos procesadores de esos líquidos (riñones, vejiga, glándulas linfáticas). Si los dos chakras de la esfera “tierra” (básico y del ombligo) no estuviesen abiertos en todos sus aspectos, los otros chakras no podrán abrirse completamente, y funcionarán de un modo muy restringido. Esos temores pueden perjudicar la experimentación de placer material, en un sentido amplio, y el gozo pleno de la vida.

En el cuerpo, está dirigido hacia los órganos reproductivos; las glándulas correspondientes son los ovarios en la mujer, y los testículos y la próstata en el hombre. Su color es naranja.

### Chakra umbilical - 2do. Chakra
| Concepto | Valor |
| -- | -- |
| Nombre | Svadhishthana |
| Localización | Zona del ombligo |
| Color | Naranja |
| Cuerpo áurico | Emocional |
| Elemento | Agua |
| Nota musical | Re |
| Mantra | Vam |
| Número de pétalos | 6 |