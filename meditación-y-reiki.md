# Meditación y Reiki

Meditar, en el significado común y popular, significa pensar en algo concreto, a fin de comprender un significado con profundidad. En el mundo occidental, meditar significa concentrarse en un pensamiento, en una palabra, o en una situación, descartando taxativamente cualquier otra reflexión, con el objetivo de llegar a un estado alterado de conciencia.

La meditación es una aventura; la mayor aventura que la mente humana puede alcanzar. Los adolescentes acostumbran fascinarse con la sexualidad; los más viejos, con el dinero y los bienes materiales pero, con el tiempo, descubren que todo eso no les proporciona la felicidad plena, y comienzan a buscarla en la espiritualidad. La meditación no es una fuga de los problemas económicos y sociales: debe ser sinónimo de alegría para todos, nunca de cansancio ni aburrimiento.

La meditación hace que nuestro ser se encuentre en armonía con el universo.

Esos resultados pueden obtenerse a través de numerosas técnicas, algunas de origen occidental.

En la tradición oriental, meditación significa no hacer nada, para llegar a un estado de perfecta paz interior, a un estado especial en el que la mente se encuentra ausente, silenciosa. Una situación en la que se experimenta una indescriptible sensación de paz y felicidad profundas.

El estado meditativo es muy personal, y ocurre en cada uno de nosotros de manera única, resultando difícil a veces intentar una descripción de lo ocurrido.

Con la meditación nos sentiremos más tranquilos, más conscientes, dormiremos mejor, nos cansaremos menos, y nuestra aura comenzará a vibrar de una manera más armónica, reflejando un crecimiento espiritual, en una manera más fácil y distinta de relacionarnos con nuestros semejantes, elevaremos nuestro nivel inmunológico, haciendo que las células del cuerpo trabajen de manera uniforme y equilibrada. El Reiki puede ser un camino para realizar meditación profunda.

Al accionar el Reiki después de las meditaciones, sentiremos una diferencia significativa por encontrarnos más próximos y en contacto más estrecho con el universo y, por lo tanto, con la energía universal.

La meditación presupone una serie de cosas que son comunes a todos los métodos. La primera norma para meditar es un cuerpo relajado, sin controlar la mente y sin concentrarse. Los ojos deben permanecer cerrados, pues el 85 por 100 de nuestro contacto con el exterior se hace a través de los ojos. Es preferible encontrar una posición cómoda que tener que cambiarla durante el proceso. La segunda es limitarse a observar la mente, un pensamiento, como si fuese una película en la que solamente somos observadores, sin interferir, sea lo que fuese.

Observar la mente, sin juicio alguno y sin critica.

La meditación es el simple existir sin hacer nada, sin acción, sin pensamiento, sin emoción, en ausencia de crítica y juicio; y, lentamente, se posesionará de nosotros un profundo silencio.

Esos son los tres puntos principales: relajamiento, observación y ausencia de crítica.

# Meditación del árbol

Siéntese en una posición confortable, respire despacio y profundamente, cierre los ojos. Visualice un árbol delante de usted, sienta su energía.

Conviértase en ese árbol. Perciba que ese árbol posee un tronco largo. Advierta las ramas y las hojas. Sienta las raíces de ese árbol al penetrar en el suelo, y la energía de la tierra que es emanada en su dirección y lo está envolviendo.

Ahora las raíces penetran más profundamente hasta llegar a un río subterráneo; es un riachuelo de aguas translúcidas y límpidas. El riachuelo baña sus raíces, llevándose todos sus miedos, rabia, limitaciones, tristeza. Una luz dorada penetra sus raíces, trayendo una sensación de paz, bienestar y equilibrio.

Ahora desplace su mente nuevamente hacia el tronco del árbol y sienta que está expandiéndose hacia arriba, pasando más allá de las nubes, llegando a las estrellas.

Sienta que la energía de la cual está hecha la estrella es la misma que la de su cuerpo. Siéntase en comunión con las estrellas, con el universo.

Ahora, del cosmos emana una luz blanca que lo envuelve; sienta ese energía.

Regrese enseguida al tronco. Perciba, a su vez, la naturaleza, la vegetación, otros árboles, los pájaros y otros seres pequeños.

Hágase, a su vez, totalmente consciente de todas las formas de vida y comparta con ellas la experiencia que ha tenido. Transmita hacia todos los seres la energía de amor y comunión, divídala. Esa energía es inagotable.

Regrese, lentamente, hacia su cuerpo. Mueva los pies, las manos, las piernas, abra y cierre los ojos, hasta sentir que ha entrado perfectamente en el cuerpo.