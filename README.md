# Introducción

Famvec nació de la experiencia recogida en más de 20 años de trabajo como terapeuta emocional y la importancia de hacer, crear y diseñar, como herramientas para tener una vida más plena y feliz.

Está reconocida mundialmente la importancia de las labores manuales como medios altamente efectivos para eliminar el stress, mejorar la salud y mantener una mente activa y sana, todo esto asociado a los nuevos y recientes descubrimientos realizados por las neuro-ciencias, la física cuántica y el reconocimiento de técnicas ancestrales de medicina provenientes, principalmente, de oriente.

La medicina de la mente y el cuerpo utiliza una variedad de técnicas diseñadas con el fin de afianzar la capacidad de la mente para afectar la función y los síntomas corporales. Algunas técnicas que se consideraron medicina complementaria y alternativa anteriormente se han formalizado \(por ejemplo, grupos de apoyo a pacientes y terapia cognitiva y conductual\). Otras técnicas para la mente y el cuerpo aún se consideran medicina complementaria y alternativa, incluida la meditación, la oración, la curación mental y las terapias que emplean soluciones creativas como el arte, la música o la danza.

Reiki, del japonés rei\(energía universal\) y ki \(energía vital\) es una práctica considerada como pseudomedicina complementaria y alternativa \(CAM\) es decir, no científica, que trata de lograr la sanación o equilibrio del paciente a través de la imposición de las manos del practicante, canalizando cierta "energía vital universal".

Jikiden Reiki es Reiki originado en Japón. Jikiden quiere decir "directamente transmitido" y los seminarios de Jikiden Reiki se basan en lo que Chiyoko Yamaguchi ha aprendido directamente de Chujiro Hayashi en el año 1938 y practicado como parte de su vida por más de 65 años. 
	
Reiki en sus orígenes daba mucha importancia al tratamiento de enfermedades y dolencias. Jikiden Reiki conserva esta característica y es muy efectivo en este tipo de tratamientos. Durante los seminarios damos mucha importancia al tratamiento como lo era en el Reiki en sus orígenes, y revelamos la esencia de las enseñanzas de Chujiro Hayachi algunos de los cuales se han perdido o modificado en el Reiki difundido en occidente. Chiyoko Yamaguchi fue una de los últimos estudiantes de Chujiro Hayashi. Aproveche esta oportunidad y conozca la autenticidad del Reiki.

El presente libro es un compendio de los textos recopilados por mi, además del conocimiento entregado por mi maestro. Espero que su lectura pueda disfrutarlo tanto como yo disfruté escribiendo este libro.

