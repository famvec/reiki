# Posiciones para la aplicación de Reiki

## Posición 1
![](/assets/posicion001.png)

#### a. Cuerpo físico

* Trabaja cualquier problema con los ojos, visión, colores, claridad (fotofobia), glaucoma, cataratas, lesiones, irritaciones y conjuntivitis.
* Problemas en la nariz, rinitis alérgica, carne esponjosa, desvío del septo, congestión respiratoria.
* Problemas con los maxilares, mandíbula, encías, dientes, pH de las mucosas y de la boca.
* Problemas en la cavidad ósea (sinusitis).
* Dolor de cabeza, jaqueca, derrames, alergia, resfriados y asma.
* Equilibra la glándula pituitaria, que también se denomina hipófisis. Ésta se localiza en el centro del cráneo, sobre la silla turca.

Se la considera la glándula principal, pues tiene como función el equilibrio del sistema de todo el cuerpo y “le dice” a las otras glándulas lo que deben hacer.

La glándula pituitaria es la glándula-maestra del sistema endocrino; influye en el crecimiento, en el desenvolvimiento sexual, en la fatiga, en la gravidez, en la lactancia, en el metabolismo, en la dosificación del azúcar y minerales en la sangre, en la retención de fluidos y en los niveles de energía.

Equilibra la glándula pineal, que también se denomina epífisis; esa glándula se localiza a la altura de la base del cráneo, es pequeña, del tamaño de un guisante, responde a los niveles de luz que los ojos perciben, gracias a la secreción de la hormona melatonina. Tiene un papel importante en el estado de ánimo. Muchos hacen referencia a esa glándula, llamándola el tercer ojo, glándula de la intuición o de la paranormalidad.

#### b. Cuerpo emocional

* Reduce el estrés.
* Alivia la ansiedad.
* Proporciona relajamiento, incluso a nivel neurológico.

#### c. Cuerpo mental

* Alivia y disminuye la confusión mental, generando equilibrio y claridad de pensamientos e ideas.
* Permite aumentar la capacidad de concentración y centralización del individuo.

#### d. Cuerpo espiritual

* Equilibra el sexto chakra.
* Permite que penetremos en nuestro yo interior, para estar en contacto con nuestra propia sabiduría.
* Nos abre hacia energías superiores.
* Permite perder la sensación de dualidad y alcanzar la sensación de unicidad con las leyes divinas.
* Amplía y ayuda a purificar la conciencia.
* Beneficia el plano de la devoción espiritual, favoreciendo la meditación y el estado de concentración.

## Posición 2
![](/assets/posicion002.png)

#### a. Cuerpo físico

* Trabaja directamente con el cerebro, equilibrando el lado derecho y el izquierdo, incentivando la producción, la creatividad, los pensamientos y la memoria.
* Equilibra las glándulas hipófisis y pineal.
* Trabaja la disritmia cerebral, convulsiones y aneurismas.
* Alivia rápidamente dolores de cabeza y jaqueca.
* Auxilia en la recuperación de personas que estén drogadas o alcoholizadas.

#### b. Cuerpo emocional

* Reduce preocupaciones, histeria y estrés.
* Ayuda a aliviar la depresión, la angustia y los miedos (todos los estados patológicos de pánico).
* Promueve el relajamiento.
* Equilibra a la persona en casos en los que predominan la emoción o el raciocinio.

#### c. Cuerpo mental

* Trabaja enfermedades mentales (psicosis, neurosis, esquizofrenia).
* Desarrolla la claridad de pensamientos, la serenidad, estimula la rapidez de las respuestas.
* Estimula una visión más clara de la vida y de los problemas.

#### d. Cuerpo espiritual

* Aumenta la capacidad de recibir energías superiores.
* Expande la conciencia y la interacción con la sabiduría cósmica (registro akásico).
* Promueve el recuerdo de sueños y vidas anteriores (insights).

## Posición 3
![](/assets/posicion003.png)

#### a. Cuerpo físico

* Armoniza el funcionamiento de la glándula pituitaria o hipófisis.
* Trabaja con la médula y el cerebro.
* Cubre la base del cerebro, armonizando las funciones desempeñadas por el cerebelo, que se encuentra en la parte posterior de la cavidad craneal.
* Disminuye la tensión del cuello y relaja la parte superior de las vértebras cervicales.
* Regula el sueño, ayuda a dormir por falta de sueño o a despertarse por exceso de éste.
* Trabaja el lóbulo occipital que se encuentra en la parte posterior del cerebro, donde se localizan los centros de la visión.
* Regula el peso y el hambre.
* Actúa en problemas relacionados con el habla y la tartamudez.
* Alivia los dolores de cabeza en la base del cráneo.
* Trabaja con personas que están en estado de conmoción por accidente, en coma o desmayadas.
* Trabaja sobre cualquier vicio, disminuyendo la compulsión.
* Trabaja la coordinación y el equilibrio (laberintitis).

#### b. Cuerpo emocional

* Desarrolla el bienestar, relajamiento y tranquiliza los pensamientos.
* Disminuye el estrés, la depresión, las irritaciones, las preocupaciones, los temores y los traumas.

#### c. Cuerpo mental

* Claridad de expresión de pensamiento e ideas.
* Promueve la serenidad, la creatividad y la productividad.

#### d. Cuerpo espiritual

* Trabaja el sexto chakra (Ajna), en su parte posterior.
* Expande la recepción de energías superiores.
* Propicia el recuerdo de sueños y vidas pasadas.
* Aberturas del tercer ojo, desarrollando los instintos (ojos y oídos internos) y la paranormalidad (capacidad de entrar en estado alterado de conciencia,
* proyección astral, clarividencia, clariaudiencia, telepatía, psicografía, etc.).

## Posición 4
![](/assets/posicion004.png)

#### a. Cuerpo físico

* Trabaja con el metabolismo, las glándulas tiroides y paratiroides. La glándula tiroides está localizada en el tercio inferior del cuello, delante de la tráquea. Regula el metabolismo y el crecimiento. Las glándulas paratiroides consisten en cuatro diminutos corpúsculos ligados a la tiroides. Controlan el metabolismo del calcio, contribuyendo al control del tono muscular.
* Trabaja los maxilares, mandíbulas, amígdalas, garganta y faringe.
* Trabaja las glándulas salivares.
* Trabaja el drenaje linfático y los ganglios cervicales superiores.
* Equilibra la presión sanguínea (alta y baja).
* La garganta es un centro de la expresión, creatividad y comunicación.

#### b. Cuerpo emocional

* Trabaja neutralizando sentimientos como la rabia, hostilidad, resentimientos, nerviosismo y miedos al fracaso.
* Desarrolla la autoestima y la autoconfianza.

#### c. Cuerpo mental

* Desarrolla la calma, relajamiento, disminución del sentido crítico, bienestar, claridad, estabilidad mental, tranquilidad y placer de vivir.

#### d. Cuerpo espiritual
* Trabaja el quinto chakra (laríngeo o Vishuda).
* Ayuda a mantener una sintonía con la espiritualidad de forma más creativa y sincera.

## Posición 5
![](/assets/posicion005.png)

#### a. Cuerpo físico

* Trabaja con el corazón, circulación, venas y arterias que salen del corazón.
* Armoniza los pulmones en la parte superior y las funciones de los bronquios.
* Cubre parte de la tráquea.
* Ayuda en el drenaje linfático.
* Equilibra el timo que, en la infancia, desempeña importantes funciones endocrinas e inmunológicas. A pesar de que se vea reducido en el adulto, su influencia sobre el organismo sigue sintiéndose, en lo que concierne a la inmunología.

#### b. Cuerpo emocional

* Esta zona es el centro energético emocional del cuerpo que, estando equilibrada, controla el envejecimiento, evitando el envejecimiento precoz.
* Trabaja los sentimientos de rabia, resentimientos, celos, amargura y hostilidad.
* Reduce el estrés.
* Desarrolla felicidad, autoconfianza, placer y armonía.

#### c. Cuerpo mental

* Desarrolla serenidad, centralización, tranquilidad, relajamiento y calma para que podamos afrontar los problemas cotidianos.

#### d. Cuerpo espiritual

* Desarrolla el amor incondicional a los semejantes y al mundo.

## Posición 6
![](/assets/posicion006.png)

#### a. Cuerpo físico

* Equilibra las funciones del hígado, estómago, bazo, vesícula biliar, páncreas y diafragma.

#### b. Cuerpo emocional

* Alivia el estrés.
* Genera relajamiento, seguridad y sentimiento de satisfacción.
* Posición importante para periodos de cambios bruscos de vida, haciendo que aceptemos ideas diferentes.

#### c. Cuerpo mental

* Genera centralización, calma, serenidad, relajamiento y claridad. La mente, al estar equilibrada, hace que funcionen mejor los órganos de la digestión.

#### d. Cuerpo espiritual

* Equilibra el chakra del plexo solar, aumentando nuestra resignación y gratitud hacia lo que se es y hacia lo que se tiene. Genera facilidad de compartir nuestro mundo físico con otras personas.

## Posición 7
![](/assets/posicion007.png)

#### a. Cuerpo físico

* Trabaja equilibrando las funciones del páncreas, vejiga, sistema reproductor (ovario, útero, trompas), apéndice, intestino delgado, duodeno y colon, parte inferior del hígado, bazo y vesícula biliar.

#### b. Cuerpo emocional

* Reduce el estrés, histeria, frustraciones, ansiedad, miedos, depresión, amargura y represión de los sentimientos.
* Mejora la autoestima y la autoconfianza.

#### c. Cuerpo mental

* Disminuye la confusión mental y el desequilibrio.

#### d. Cuerpo espiritual

* Equilibra el chakra del ombligo.

## Posición 8
![](/assets/posicion008.png)

#### a. Cuerpo físico

* Trabaja con la vesícula, intestinos, ovarios, útero, próstata, vagina, energía sexual (orgasmo).

#### b. Cuerpo emocional

* Desarrolla respuestas emocionales saludables ante la vida sexual, rompiendo patrones y pensamientos rígidos relacionados con la sexualidad.
* Reducción de la ansiedad, el nerviosismo y el pánico.
* Trabaja todo tipo de vicio.

#### c. Cuerpo mental

* Promueve la creatividad, mejorando la flexibilidad y la capacidad de adaptación.

#### d. Cuerpo espiritual

* Equilibra el chakra básico.

## Posición 9
![](/assets/posicion009.png)

#### a. Cuerpo físico

* Trabaja tensiones y contracturas frecuentes de los músculos trapecio y lumbar.
* Trabaja la columna vertebral, sistema nervioso, problemas en los pulmones y enfermedades alérgico-respiratorias.

#### b. Cuerpo emocional

* Promueve la reducción del estrés, el relajamiento, la disminución de tensiones, generando autoconfianza y tranquilidad.

#### c. Cuerpo mental

* Desarrolla serenidad, centralización y estabilidad.

#### d. Cuerpo espiritual

* Favorece la recepción de energías superiores.

## Posición 10
![](/assets/posicion010.png)

#### a. Cuerpo físico

* Equilibra las funciones del hígado, estómago, bazo, vesícula biliar, páncreas y diafragma.

#### b. Cuerpo emocional

* Alivia el estrés.
* Genera relajamiento, seguridad y sentimiento de satisfacción.
* Posición importante para periodos de cambios bruscos de vida, haciendo que aceptemos ideas diferentes.

#### c. Cuerpo mental

* Genera centralización, calma, serenidad, relajamiento y claridad. La mente, al estar equilibrada, hace que funcionen mejor los órganos de la digestión.

#### d. Cuerpo espiritual

* Equilibra el chakra del plexo solar, aumentando nuestra resignación y gratitud hacia lo que se es y hacia lo que se tiene. Genera facilidad de compartir nuestro mundo físico con otras personas.

## Posición 11
![](/assets/posicion011.png)

#### a. Cuerpo físico

* Trabaja equilibrando las funciones del páncreas, vejiga, sistema reproductor (ovario, útero, trompas), apéndice, intestino delgado, duodeno y colon, parte inferior del hígado, bazo y vesícula biliar.

#### b. Cuerpo emocional

* Reduce el estrés, histeria, frustraciones, ansiedad, miedos, depresión, amargura y represión de los sentimientos.
* Mejora la autoestima y la autoconfianza.

#### c. Cuerpo mental

* Disminuye la confusión mental y el desequilibrio.

#### d. Cuerpo espiritual

* Equilibra el chakra del ombligo.

## Posición 12
![](/assets/posicion012.png)

#### a. Cuerpo físico

* Trabaja con la vesícula, intestinos, ovarios, útero, próstata, vagina, energía sexual (orgasmo).

#### b. Cuerpo emocional

* Desarrolla respuestas emocionales saludables ante la vida sexual, rompiendo patrones y pensamientos rígidos relacionados con la sexualidad.
* Reducción de la ansiedad, el nerviosismo y el pánico.
* Trabaja todo tipo de vicio.

#### c. Cuerpo mental

* Promueve la creatividad, mejorando la flexibilidad y la capacidad de adaptación.

#### d. Cuerpo espiritual

* Equilibra el chakra básico.

## Posición 13
![](/assets/posicion013.png)

#### a. Cuerpo físico

* Trabaja en la sangre, circulación, presión sanguínea, diafragma, garganta, senos, ovarios, cadera, hombros y, prácticamente, todo el resto del cuerpo.

#### b. Cuerpo emocional

* Armoniza el cuerpo áurico emocional, siendo una posición clave en momentos de dudas e indecisiones importantes.

#### c. Cuerpo mental

* Armoniza el cuerpo áurico mental, generando más equilibrio y centralización.

#### d. Cuerpo espiritual

* Armoniza el cuerpo áurico astral, equilibrando la velocidad normal de los siete chakras principales, y estimulando y coordinando los movimientos de los nadis (ida y pingala).

## Posición 14
![](/assets/posicion014.png)

En los pies tenemos los puntos reflejos que están conectados con otras zonas y órganos del cuerpo, que, al ser irradiados por el Reiki, desbloquean los canales eléctricos de unión, facilitando el fluir de la energía. Podemos comenzar o terminar el tratamiento por los pies.

#### a. Cuerpo físico

* En la planta de los pies trabajamos prácticamente todas las glándulas y órganos del cuerpo.

#### b. Cuerpo emocional

* Armonizamos el primer cuerpo áurico (etérico) y el cuerpo áurico emocional.

![](/assets/posicion015.png)