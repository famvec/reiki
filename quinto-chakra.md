# Quinto chakra o chakra de la laringe

Se localiza en medio de la garganta, próximo a la zona designada como“manzana de Adán”, está abierto hacia delante, y tiene también un vórtice trasero.

Es el chakra de la comunicación externa y el comienzo de la comunicación interna (clariaudiencia) y autoexpresión; gobierna la postura del cuerpo.

El chakra laríngeo es el chakra de la comunicación, de la creatividad, del sonido y de la vibración, de la capacidad de recibir y asimilar, y se relaciona con los sentidos del paladar, audición y olfato, y es el umbral de la alta conciencia y de la purificación, y es por medio del trabajo de este chakra como podemos iniciar el camino espiritual; en consecuencia, como nos ponemos en comunicación con nuestra esencia superior.

Cuando está abierto y armonizado, somos conscientes de la responsabilidad de nuestro desarrollo en todos los sentidos, desde nuestras necesidades materiales hasta las espirituales. Nos facilita la comprensión de cuál es nuestro papel en la sociedad y en el trabajo, y nos ocupamos en conseguir el máximo posible de satisfacción.

Es el centro psicológico de la evolución de la creatividad, responsabilidad, iniciativa y autodisciplina.

Cuando está en desarmonía, aparece el miedo de la desaprobación social de nuestros semejantes, miedo al fracaso en la vida social, y nos convertimos en seres potencialmente agresivos; adoptando una actitud instintiva de defensa propia, podemos ser llevados a escondernos en el orgullo para poder soportar la carencia de éxito.

Su desequilibrio produce patologías tales como: 

susceptibilidades a las infecciones virales o bacterianas (amigdalitis, faringitis), resfriados, herpes, dolores musculares o de cabeza, en la base del cráneo (nuca), con gestión linfática, problemas dentales y endurecimiento de los maxilares (bruxismo).

Los bloqueos en el chakra laríngeo producen frecuentemente síntomas físicos como la ronquera (“no consigo hablar mucho tiempo sin quedar ronco”), la persona tiene dificultad de comunicarse, tartamudea, sus palabras son atropelladas, su cabeza está caída hacia abajo, su mandíbula se orienta en la dirección de la laringe.

Cuando existe hiperactividad de este chakra, el individuo es ronco, habla con voz aguda y estridente y puede transformarse en un demagogo; discute sólo por discutir, gusta de discutir, quiere cambiar al mundo de acuerdo con sus ideales; el individuo tenderá a mantener la cabeza erguida con la nariz “hacia el aire”.

Ese chakra participa de cualquier desequilibrio psicofísico; su centro físico corresponde a la tiroides, que desempeña un papel importante en el crecimiento del esqueleto y de los órganos internos, regulando el metabolismo, regula el yodo y el calcio en la sangre y en los tejidos. Su energía también es responsable de la parte inferior del rostro, nariz y aparato respiratorio, tráquea, esófago, cuerdas vocales, laringe y sistema linfático. Su color es el azul, su elemento el éter y el sonido es el Ham.

### Chakra del plexo solar - 5do. Chakra
| Concepto | Valor |
| -- | -- |
| Nombre | Manipura |
| Localización | Plexo solar |
| Color | Amarilo |
| Cuerpo áurico | Mental |
| Elemento | Fuego |
| Nota musical | Mi |
| Mantra | Ram |
| Número de pétalos | 16 |