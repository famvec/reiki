# EL CAMPO ENERGÉTICO HUMANO O AURA HUMANA

El campo energético humano es una manifestación de energía universal íntimamente vinculada con la vida humana. Usualmente se denomina aura, y ha sido descrito como un ente luminoso que rodea el cuerpo físico y penetra en él, y que emite su propia radiación característica. El aura es la parte del CEU asociada con los objetos. El aura humana, o campo energético humano \(CEH\), es la parte del CEU relacionada con el cuerpo humano. Basándose en sus observaciones, los investigadores han creado modelos teóricos que dividen el aura en varias capas, a veces denominadas cuerpos, que se interpenetran y rodean mutuamente en capas sucesivas. Cada cuerpo subsiguiente está compuesto por sustancias más finas y «vibraciones» más altas que el cuerpo al que rodea y en el que penetra.

## Posición de los chakras \(Vista diagnóstica\)

![](/assets/001)

## Chakras mayores, en vista normal y posterior \(diagnóstica\)

![](/assets/002)

## Sistema del cuerpo aural de siete capas \(vista de diagnóstico\)

![](/assets/003)

Es importante abrir los chakras y aumentar nuestro flujo energético, ya que cuanta más energía dejemos fluir más sanos nos encontraremos. La enfermedad del sistema la provoca un desequilibrio de energía o un bloqueo del flujo energético. Dicho de otro modo, la falta de flujo en el sistema energético humano conduce indefectiblemente a la enfermedad, además de deformar nuestras percepciones y embotar nuestras sensaciones, interfiriendo así en una experiencia serena de vida plena.

