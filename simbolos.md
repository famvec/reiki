# El ideograma

La palabra Reiki puede escribirse con ideogramas japoneses, que, lo mismo que ocurre con los guarismos romanos, no expresan letra ni sonido, y sí una idea.

Según el contexto, esos ideogramas pueden ofrecer varias lecturas, con los siguientes significados:

I. Lluvia maravillosa de energía vital.

II. Lluvia maravillosa que da vida.

III. La idea de algo que procede del cosmos y que, en su encuentro con la Tierra, produce el milagro de la vida.

IV. Lluvia maravillosa que produce el milagro de la vida.

V. La comunión de una energía superior con una de orden terreno, aunque se pertenecen mutuamente.

VI. Una energía maravillosa que se encuentra por encima de todas las demás, y que también está en usted, y que usted pertenece a ella.

En algunos casos, ese ideograma se encuentra reforzado con pequeñas formas que representan granos de arroz, que simbolizan la vida.

![](/assets/006)

# El color
El color simbólico del Reiki es el verde, que es el color de la curación, así como del amor; guarda correlación con el chakra cardíaco, responsable por nuestro amor incondicional y por el sistema inmunológico.

Sus ideogramas son hechos en dorado, pues ese es el color cósmico; Reiki es luz que nos lleva de regreso a la gran luz.

# El Bambú
El Reiki tomó de la naturaleza, como símbolo, el bambú que, en su simplicidad, resistencia al viento (cuando sopla fuerte), vacío, rectitud y perfección, puede representar, metafóricamente, el funcionamiento de la energía.

El bambú es flexible a pesar de ser fuerte; reverencia al viento que lo roza cuando sopla, se dobla a la vida, mostrándonos que cuanto menos se oponga un ser a la realidad de la vida, más resistente se volverá para vivir con plenitud. El bambú es fuerte, y sirve para la construcción de embarcaciones, muebles y edificaciones, es decir, todos los que recibieron el Reiki tienden a permanecer fuertes y resistentes.

Entre un nudo y otro, el bambú es hueco, vacío; como vacío es el espacio entre el cielo y la tierra, representando los que escogieron ser canales de Reiki, los cuales pasan a funcionar en ese vacío como verdaderos “tubos” canalizadores de energía cósmica.

La rectitud sin igual del bambú, la perfección de su proyectarse hacia las alturas, así como sus nudos, los cuales simbolizan las diferentes etapas del camino, simbolizan el objetivo de nuestro itinerario interior, de nuestro crecimiento y de la evolución en dirección a la meta.

En el Japón, el bambú es una planta de buenos auspicios, de buena suerte; pintar el bambú es considerado no sólo arte, sino también un ejercicio espiritual. En algunas culturas africanas, el bambú es un símbolo de alegría, de felicidad, de vivir sin enfermedades ni preocupaciones, y es interesante observar cómo esa simbología tiene que ver con los principios del Reiki.