# Definición de Reiki
Reiki es una palabra japonesa que significa energía vital universal; en la actualidad, esa palabra se está utilizando para identificar el Sistema Usui de Curación Natural (Usui Shiki Ryoho), nombre dado en homenaje a su descubridor, Mikao Usui.

Reí significa universal y se refiere a la parte espiritual, a la esencia energética cósmica, que interpenetra todas las cosas y circunda todos los lugares.

Ki es la energía vital individual que rodea nuestros cuerpos, manteniéndolos vivos, y está presente, fluyendo, en todos los organismos vivos; cuando la energía Ki sale de un cuerpo, ese cuerpo deja de tener vida.

El Reiki es un proceso de encuentro de esas dos energías: la energía universal con nuestra porción física, y ocurre después de que la persona es sometida a un proceso de sintonización o iniciación en el método, hecho por un maestro capacitado.

El Reiki es una energía semejante a ondas de radio, y puede ser aplicada con eficacia, tanto localmente como a distancia; no es como la electricidad, no produce cortocircuitos, no destruye los nervios ni los tejidos más frágiles. Es una energía inofensiva, sin efectos secundarios, sin contraindicaciones, compatible con cualquier tipo de terapia o tratamiento. Es práctica, segura y eficiente, y, por medio de la técnica, equilibra los siete chakras o centros de fuerza sutil de energía, localizados entre la base de la columna y la parte superior de la cabeza.

Cuando hacemos uso de la energía Reiki estamos aplicando energía-luz, tratando de recuperar y mantener la salud física, la mental, la emocional y la espiritual; es un método natural de equilibrar, restaurar, perfeccionar y curar los cuerpos, creándole un estado de armonía al ser.