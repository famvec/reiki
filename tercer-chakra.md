# Tercer chakra o chakra del plexo solar

Se localiza en la zona del diafragma, en la boca del estómago, un poco por encima del estómago, ligeramente a la izquierda. Está abierto hacia delante, y posee también un vórtice posterior.

Representa la personalidad, y en él están concentradas las cualidades de la mente racional y personal, de la vitalidad, de la voluntad de saber y aprender, de la acción de poder, del deseo de vivir, de comunicar y participar. Es el punto de conexión con otras personas. Se trata de un chakra poderoso, que promueve la autoaceptación. A través de su plena armonía encontramos y vivimos con plenitud nuestros atributos físicos y mentales; nos movemos en la sociedad con desenvoltura y armonía.

El tercer chakra es el que más se relaciona con nuestro ego, y por eso absorbe mucha energía de los dos primeros.

Voluntad y poder representan para nosotros, en la sociedad actual, una llave del éxito, pero también puede implicar que, con el deseo de mejorar de posición social, lleguemos a menospreciar a nuestros semejantes, imponiéndonos sobre los demás, con el objetivo de obtener lo que nos interesa. El egoísmo obstruiría, desequilibrando o desarmonizando los chakras superiores, y, consecuentemente, echaría a perder nuestro proceso evolutivo.

Ese chakra controla el estómago, la musculatura abdominal, el hígado, la vesícula, el bazo y el páncreas, las secreciones gástricas desordenadas y las disfunciones de las glándulas salivares.

Si el chakra estuviese falto de armonía, podrá alimentarse un sentimiento de inferioridad, y podrán disminuir las capacidades mentales tales como la lógica y la razón, aumentando, como consecuencia de ello, la confusión y el sentimiento de inseguridad, con lo que la persona puede generar patologías, tales como diabetes, desórdenes en el tracto digestivo, alergias, sinusitis, insomnio, además de la separación entre amor y sexo.

Los bloqueos en el plexo solar acaban frecuentemente en síntomas y actitudes mentales, como pretensiones de poder y control (“mi marido”, “mi mujer”, “mi hijo”, “mi dinero”), ambición (“la vida no tiene valor si no logro una función más elevada”, “un empleo mejor”, “una amante”, “si no cambio el coche todos los años”), gasto compulsivo (“¡necesito desesperadamente joyas o ropas nuevas!”), ansiedad de consideración (“¿qué voy a hacer si mi patrón me despide?”, “si no apruebo las oposiciones”, “¡si tuviese que vender el coche nuevo!”), y de envidia (“¡Ese individuo tiene un BMW nuevo!”).

Su centro físico corresponde al páncreas, cuya función es la transformación y digestión de los alimentos; el páncreas produce la hormona insulina, equilibradora del azúcar en la sangre, y transforma los hidratos de carbono que, además de aislar las enzimas, son importantes para la asimilación de grasas y proteínas. El color de este chakra es amarillo, su elemento es el fuego y su sonido es Ram.

### Chakra del plexo solar - 3do. Chakra
| Concepto | Valor |
| -- | -- |
| Nombre | Manipura |
| Localización | Plexo solar |
| Color | Amarilo |
| Cuerpo áurico | Mental |
| Elemento | Fuego |
| Nota musical | Mi |
| Mantra | Ram |
| Número de pétalos | 10 |
