# Sexto chakra o chakra frontal
El chakra frontal es el chakra de los sentidos, y es responsable por la energía de la parte superior de la cabeza, (encima de la nariz), parte craneal, ojos y oídos.

El chakra frontal, el tercer ojo, se localiza en medio de la frente, en el entrecejo, justo encima del nivel de los ojos; está abierto hacia delante, teniendo también un vórtice trasero. El sexto chakra representa la intuición, la evidencia y la audiencia en el campo de la paranormalidad.

Cuando está en desarmonía puede afectar esos órganos, además de colocarnos en una situación confusa, en la que las ideas y los conceptos no tendrán una correspondencia con la realidad, obstruyendo nuestras ideas creativas; nos quedamos sin raciocinio lógico y sin capacidad de poner en práctica nuestras ideas.

Percepción, conocimiento y liderazgo son prerrogativas de este chakra, que nos permiten entrar en el mundo del lo aparentemente invisible mediante la percepción extrasensorial; a través de él, también emitimos nuestra energía mental.

Actúa directamente sobre la pituitaria (hipófisis), que dirige la función de las demás glándulas.

Su color es el azul índigo, está ligado al cuerpo áurico mental, no tiene elemento correspondiente en el mundo físico, y su sonido es el Om.

Los bloqueos en el chakra frontal son motivados por su hiperactividad y causan síntomas tales como falta de objetivo, inestabilidad de vida (“¡no sé por qué vivo!”), alienación del trabajo (“¡no importa el trabajo, siempre que se gane un buen salario!”) y miedo a las apariciones, espíritus, fantasmas, etc.

Algunos otros síntomas típicos son: desempleo permanente (inestabilidad profesional), cambios de residencia constantes, relevo continuo de compañeros amorosos, vestirse de acuerdo con la tendencia de la última moda, adoración de ídolos, fanatismos y hechos semejantes, falta de opinión, ausencia total de interés por cualquier cosa, y falta de iniciativa.

La afirmación “No encuentra su camino”, resume la condición de un chakra frontal desordenado.

Cuando se halla en desequilibrio, produce patologías tales como: vicios demdroga, alcohol, compulsiones, problemas en los ojos (ceguera, catarata), sordera.

### Chakra frontal - 6to. Chakra
| Concepto | Valor |
| -- | -- |
| Nombre | Ajna |
| Localización | En el entrecejo |
| Color | Azul indigo |
| Cuerpo áurico | Celestial |
| Elemento | Fuego |
| Nota musical | Mi |
| Mantra | Ram |
| Número de pétalos | 96 |
