# Cómo funciona el Reiki
La cultura occidental está basada en una concepción newtoniana-cartesiana, que apuesta por el estudio de las partes para llegar al todo. Esta concepción se encuentra hoy muy cuestionada; la propia física cuántica, a través de investigaciones sobre el átomo y la energía nuclear, demuestra que, en el nivel más ínfimo, la materia es al mismo tiempo energía.

 Los científicos modernos han analizado el mundo con un grado increíble de sofisticación. El mundo material está dividido en partículas cada vez más pequeñas y, al final, lo que encontramos son ondas de energía (cuantos). Descubrimos la verdad simple de que la energía precede a la materia, así como las emociones y pensamientos preceden a la acción.

Esa visión del mundo, nueva en Occidente, antiquísima en el Oriente, propone que todo lo que existe es energía. La energía es la realidad básica que se condensa, se equilibra y forma la materia.

Con la formula moderna de Albert Einstein (E = mc2) quedó probado científicamente que materia y energía son convertibles e intercambiables. Por ejemplo, los elementos plutonio y uranio enriquecidos pueden ser transformados en energía pura (explosiones), como ocurrió en Hiroshima y Nagasaki; y también que se puede transformar la energía en materia, ya que son dimensiones de la misma realidad.

Desde los tiempos de las medicinas china, tibetana e india, e incluso desde la época de los alquimistas medievales, existen técnicas milenarias que nos enseñan que la materia, efectivamente, se transforma y puede ser moldeada con la intervención de una energía mayor.

La energía es energía; no existe energía mala; solamente existe energía bien o mal dirigida. En una persona sana, la energía atraviesa libremente por nuestro cuerpo físico, fluyendo por “caminos”: chakras, meridianos energéticos y nadis.

También rodea al campo energético, al cual denominamos aura. Esa fuerza energética nutre nuestros órganos y células, y regula las funciones vitales; cuando se bloquea esa energía y se interrumpe la circulación de esa energía, ocurre una disfunción en los órganos y tejidos de nuestro cuerpo.

En virtud de excesos físicos, emocionales, mentales y espirituales, liberamos energías, y esas liberaciones generan “nudos energéticos” o “bloqueos energéticos” que interrumpen o impiden el flujo normal de la energía vital, originando una disfunción en los órganos y tejidos del cuerpo, lo que, en consecuencia, causa la enfermedad, en razón del funcionamiento deficiente o el mal funcionamiento de los órganos y de las glándulas.

El Reiki cura al pasar a través de la parte afectada de nuestro campo energético, elevando el nivel vibratorio dentro y fuera de nuestro cuerpo físico, donde se alojan sentimientos y pensamientos en forma de nódulos energéticos, que actúan como barreras para nuestro flujo normal de energía vital; son muchos los que conviven con esas barreras a lo largo de toda una vida, reduciendo al mínimo su calidad de vida.

En una sesión de Reiki, la cantidad de energía recibida por el paciente está determinada por el propio paciente, toda vez que el terapeuta reikiano se limita a dirigir la energía y el proveedor (el Cosmos) la entrega de forma ilimitada.