# Cuarto chakra o chakra cardíaco
El chakra cardíaco se localiza en la parte superior del pecho, en la zona del corazón, ligeramente a la izquierda; está abierto hacia delante, teniendo también un vórtice posterior; representa el amor incondicional que nos permite amar enteramente y sin condiciones. Cuando está activo, nos relacionamos con todo y con todos, aceptando tanto los aspectos positivos como los negativos, y siendo capaces de dar amor sin esperar nada a cambio. Es el chakra situado en el medio; un puente de transferencia de energía entre los chakras inferiores y superiores. Es el chakra por el que pasa toda la energía que deseamos entregar a los demás.

Únicamente si está abierto y vitalizado, podremos brindar energía de curación (Reiki). Por ello, algunos reikianos, tras la activación, sienten fluir la energía con mayor intensidad, debido a que poseen un chakra cardíaco más armonizado que el de las demás personas. El centro físico de este chakra corresponde al timo, cuya función es regular el crecimiento (en los niños), dirigir el sistema linfático, estimular y fortalecer el sistema inmunológico; damos un sentido pleno a nuestra existencia si trabajamos bien este chakra de amor y compasión.

Cuando nos encontramos desequilibrados y sin armonía no somos capaces de amar; pensamos que el prójimo, el destino y Dios son incompatibles con nosotros; podemos llegar a desarrollar mecanismos violentos de respuesta a los demás. En lugar de solicitar ayuda de ellos, el lema pasa a ser: “Yo contra todos”, lo que vuelve inarmónico instantáneamente al cuarto chakra.

Los bloqueos en el chakra cardíaco devienen frecuentemente en síntomas y actitudes mentales como la imposición de condiciones al amor (“¡Si no haces lo que quiero, me voy a separar de ti!”), de amor sofocante (“¡Hijo querido, yo sólo quiero lo mejor para ti!”), de egoísmo (“¡Tienes que estar aquí en caso de que yo necesite ayuda!”).

Su desarmonía produce patologías tales como: síndrome de pánico, calambres, acidez, palpitaciones, arritmia cardiaca, rubor, presión alta, enfermedades de los pulmones, problemas con el nivel de colesterol, intoxicación, tensión y cáncer. Su color es el verde, su elemento el aire y su sonido es el yam.

### Chakra cardiaco - 4to. Chakra
| Concepto | Valor |
| -- | -- |
| Nombre | Anahata |
| Localización | Zona cardiaca|
| Color | Verde o Rosa |
| Cuerpo áurico | Astral |
| Elemento | Aire |
| Nota musical | Fa |
| Mantra | Yam |
| Número de pétalos | 12 |
