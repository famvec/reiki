# Séptimo chakra o chakra coronario

El chakra coronario está localizado en la parte superior de la cabeza; está abierto hacia arriba con un único vórtice; representa la comprensión y la conexión con energías superiores. Está asociado a la conexión de la persona con su espiritualidad y a la integración de todo el ser físico, emocional, mental y espiritual.

Es el chakra más importante, y es el eslabón entre la mente espiritual y el cerebro físico, relacionándose con nuestro ser total y con la realidad cósmica.

Tiene una forma distinta de la de los demás chakras, con intensas radiaciones luminosas y translúcidas. En virtud de hallarse exactamente en la condición de semejante al universo, al todo, al cosmos, a Dios, no tiene sonido correspondiente en el mundo físico; está hecho de silencio puro de la formación de los mundos.

Llegar a la apertura y a la plena conciencia de este chakra conduce a la perfección del ser, pero solamente se llega a ésta después de la apertura y la conciencia de todos los otros chakras.

El séptimo chakra es luz de conocimiento y conciencia; es visión global del universo; es nuestro camino de crecimiento, haciendo que podamos alcanzar la serenidad espiritual y la completa conciencia universal.

El color de este chakra es blanco, dorado o violeta; corresponde a la glándula pineal, que actúa en el organismo como un todo.

Cuando se encuentra en equilibrio nos permite experiencias muy personales; las sensaciones van más allá del mundo físico, creando en el individuo el sentido de totalidad, de paz y fe, dando un sentido propio a su existencia.

La falta de equilibrio del séptimo chakra acarrea como consecuencia una pubertad tardía, y falta de comprensión de la parte espiritual, tanto propia como ajena, y, por consiguiente, una visión materialista de la existencia. La persona no tendrá conexión con su espiritualidad y generará patologías tales como: insomnio, jaquecas, desórdenes en el sistema nervioso, histeria, posesión, obsesión, neurosis y disfunciones sensoriales.

### Chakra coronario - 7mo. Chakra
| Concepto | Valor |
| -- | -- |
| Nombre | Sahasrara |
| Localización | Parte superior de la cabeza |
| Color | Blanco, Dorado, Violeta |
| Cuerpo áurico | Causal |
| Nota musical | Si |
| Número de pétalos | 10000 |
