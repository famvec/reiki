# Antecedentes del Reiki y su redescubrimiento

El Arte de Colocar las manos sobre un cuerpo humano o animal, para reconfortar y disminuir los dolores, es un antiguo instinto humano; cuando sentimos dolores, lo primero que hacemos es colocar intuitivamente las manos sobre la zona que nos está doliendo. El toque humano distribuye calor, serenidad y curación. Cuando un animal lame una herida, está actuando bajo los mismos instintos que el ser humano cuando se coloca las manos.

Esa fuerza \(energía vital\) ha recibido distintos nombres en cada cultura: los polinesios la llaman mana; los indios iroqueses americanos, orenda; en la India se la conoce como prana; en hebreo es ruach; baraca en los países islámicos; chi en China; en el Japón, a esta energía se la conoce como ki; y para los rusos es energía bioplasmática.

En el Tíbet existen registros de técnicas de curación por medio de las manos desde hace más de ocho mil años. Esas técnicas se expandieron por Grecia, Egipto, India y otros países, a pesar de que la técnica permaneció perdida durante los últimos dos milenios.

Existen hechos que indican que Jesús practicó el Reiki en Egipto. Jesús aplicaba la técnica con mucho éxito, y también les decía a sus apóstoles “curad a los que estén enfermos”. Hasta hoy día, algunos sacerdotes católicos conservan técnicas de imposición de manos.

# Mikao Usui, el redescubridor del Método

Mikao Usui \(ver foto\), nacido en Japón el 15 de agosto de 1865; no poseemos datos oficiales detallados de su historia. Existen controversias al respecto de la vida del redescubridor del método Reiki; su historia fue transmitida oralmente de maestro a discípulo, permaneciendo envuelta en mucho misterio. Con el transcurrir de los años sufrió varias alteraciones, con el fin de que el método pudiese ser introducido en Occidente, principalmente en lo que concierne a su formación profesional y a su religiosidad; no obstante, la esencia, que puede quedar mejor descrita como una leyenda, se la conoce por haber sido transmitida de generación en generación.

![](/assets/004)

# Mikao Usui, la historia occidentalizada

Mikao se hizo sacerdote católico. Además de ser sacerdote cristiano, impartía lecciones y era rector de una pequeña universidad cristiana en Kioto \(Japón\), la Doshisha University.

Usui escuchaba y leía muchas historias sobre Jesús, que en el pasado, mediante la imposición de las manos y siguiendo una técnica específica, realizaba curaciones, milagros, y ayudaba a otras personas en sus habilidades metafísicas; curioso, observaba que una gran parte de las personas eran infelices e improductivas, asoladas por estados represivos y enfermizos; situaciones que lo indujeron ardientemente a conocer también las habilidades curativas.

Cierto día, durante una discusión con un grupo de seminaristas que concluían su formación, le preguntaron al doctor Usui si creía literalmente en la Biblia. Al responder afirmativamente, sus estudiantes le hicieron recordar las curaciones realizadas por Cristo. Los estudiantes mencionaban las palabras de Cristo: “Harás como yo he hecho, y también las cosas grandes”.

Se preguntaban por qué no existían en el mundo de hoy otros sanadores que actuasen de la misma manera que Cristo, pues Él había pedido a sus apóstoles que “curasen a los enfermos y resucitasen a los muertos”. Si eso es verdad, enséñenos los métodos, inquirieron los alumnos; queremos saber cómo podrían llevarse a cabo hoy también aquellas curaciones. Le dijeron que no era suficiente con que ellos creyeran; querían ver con sus propios ojos cómo Jesús realizaba la curación. Mikao Usui no podía dar respuesta a las dudas planteadas por los estudiantes porque no la tenía. Sin embargo, no podía quedar sin respuesta, ni para sí, ni para sus estudiantes. Usui no tenía cómo enseñar la fórmula de armonización del cuerpo tal como Jesús la transmitió a sus discípulos; simplemente tenía fe en las escrituras. El doctor Usui permaneció callado, pues, de acuerdo con la tradición japonesa, había sido ultrajado en su honra como profesor y rector, en virtud de no haber podido responder las preguntas de sus discípulos. En ese mismo día pidió dimitir de sus funciones y se decidió a buscar las respuestas a este gran misterio.

Como la mayoría de sus profesores habían sido misioneros norteamericanos, y los Estados Unidos era un país predominantemente cristiano, decidió iniciar sus estudios en la Universidad de Chicago, en el seminario teológico, auspiciado por el intercambio cultural de la dinastía Meigi.

En 1898, Mikao viajó a los Estados Unidos, donde estudió teología, cristianismo y la Biblia, y, tras siete años de estudio, se doctoró en teología.

Estudió lenguas antiguas para poder leer las antiguas escrituras, inclusive el chino y el sánscrito, la lengua más antigua de la India. Tras este largo periodo de estudios, al no haber encontrado las respuestas, decidió que debería continuar sus investigaciones en algún otro lugar.

En aquel momento, tropezó con el hecho de que Gautama el Buda \(620-543 a. de C.\) también era conocido por sus curaciones de ciegos, de enfermedades tales como la tuberculosis y la lepra, entre otras, y resolvió, por ello, regresar a Japón, a fin de investigar más sobre las curaciones realizadas por el Buda, con la esperanza de hallar la clave para la curación.

El principal centro budista se hallaba en Nara, no obstante, en Kioto había cerca de 880 templos y monasterios, e incluso un templo Zen que poseía la mayor biblioteca budista del Japón, donde podría investigar las escrituras de los Sutras referentes a las curaciones del Buda.

Durante siete años, Mikao Usui peregrinó en busca de las Antiguas Escrituras en las bibliotecas, y de monasterio en monasterio; entretanto, cada vez que tenía cerca algún monje budista, se dirigía a ellos y les preguntaba si tenían conocimiento de alguna fórmula en relación con las curaciones realizadas por el Buda, y siempre recibía la respuesta de que, en aquel momento, estaban muy ocupados con la curación del espíritu para poderse preocupar con la curación del cuerpo. Después de numerosas tentativas, llegó a un monasterio zen y, por primera vez, fue alentado por un anciano monje que estuvo de acuerdo en que podría ser posible curar el cuerpo, como ya lo había hecho el Buda; y además, que si había sido posible una vez, debería existir la posibilidad de descubrir nuevamente la fórmula de curación. Pero le advirtió que, durante muchos siglos, toda la concentración se había puesto en la curación del espíritu.

Mikao decidió que iba a estudiar los Sutras en el Tíbet y, en vista de que dominaba bien el sánscrito, viajó a la India, y en una de sus investigaciones en un antiguo manuscrito de un discípulo anónimo del Buda, escrito en ese idioma, encontró los cuatro símbolos sagrados de la fórmula utilizada por el Buda para curar.

Los Sutras, escritos hace más de 2.500 años, ponían en movimiento una energía sumamente poderosa capaz de conducir a un poder ilimitado de curación; sin embargo, una simple fórmula sin las explicaciones de cómo usarla, y sin poseer la debida capacidad de activarla, no le otorgaba la habilidad de curar.

# La meditación de Mikao Usui

En 1908, en el Japón, Mikao decidió iniciar un periodo de ayuno y meditación de veintiún días, como lo habían hecho los antiguos maestros, con el fin de purificarse para recibir una visión que lo esclareciese. Dejó, entonces, el monasterio y se retiró al Monte Kurama, la montaña sagrada, situada

aproximadamente a 25 kilómetros de Kioto, llevando los Sutras encontrados por él en el Tíbet y, escasamente, un recipiente de piel de cabra con agua y veintiuna piedras, que le servirían de calendario, arrojando cada día una de ellas. Mientras pasaban los días, Mikao, en ayuno absoluto, sentado cerca de un pino, escuchando el sonido de un riachuelo, permaneció meditando, orando, entonando cánticos, leyendo los Sutras y pidiendo al Creador que le diese el discernimiento necesario para utilizar los símbolos.

El ayuno y la meditación ampliaron las fronteras de su conciencia, y en la madrugada del vigésimo primer día, Mikao tuvo una visión en la que vislumbró una intensa luz blanca que le golpeó de frente, proyectándole fuera del cuerpo; y, sintiendo la conciencia profunda en comunicación con su “Yo” mental, al abrir totalmente su conciencia, pudo ver muchas luces en forma de burbujas coloridas que contenían en su interior símbolos sagrados, y, a través de la comunicación que estaba recibiendo, le fue dada la comprensión de los significados de los símbolos y la utilización de los mismos.

En aquel momento, Mikao recibía su iniciación, el conocimiento de cómo utilizar los símbolos y de cómo activar el poder en otras personas, rescatando así el método milenario de terapia.

# Mikao Usui y los primeros milagros del Reiki

Cuando concluyó el trance que le trajo la visión, el doctor Usui se sintió bien,

sin hambre, lleno de energía, fuerte y en total plenitud, hasta el punto de lograr caminar de regreso al monasterio. Se sentía totalmente diferente a los últimos momentos que precedieron al final de los veintiún días de meditación. No seguía sintiendo los esfuerzos del retiro y del ayuno, y se levantó con entusiasmo y comenzó a descender la montaña; ese fue el primer milagro de aquella mañana.

Durante el descenso de la montaña, con la prisa de regresar con sus revelaciones recientes al monasterio zen donde vivía, Mikao sufrió un accidente, al tropezar en una piedra, haciéndose bastante daño, hasta el punto que el pie le comenzó a sangrar y a dolerle mucho; instintivamente, Mikao impuso las manos y, en poco tiempo, se le pasó el dolor y se detuvo la hemorragia; ese fue el segundo milagro. Usui tenía consigo la clave de la armonización que tanto había buscado. El tercer milagro se produjo durante el camino de regreso al monasterio, cuando se detuvo en una posada para comer. El hombre, ya anciano, que lo atendió, viendo la longitud de su barba y el estado de sus ropas, comprendió que había permanecido en ayuno durante un largo periodo, y lo animó a comer un tipo especial de pan, ante el peligro de romper un ayuno con comida demasiado abundante. El doctor Usui rechazó la sugerencia y pidió el menú completo. Sentado en un banco bajo un árbol, se comió los alimentos sin ningún problema de digestión \(este fue el tercer milagro\).

Mikao se percató de que la nieta del hombre que le había servido estaba llorando, y que una parte de su rostro estaba hinchada y enrojecida. Le preguntó qué le estaba sucediendo, y la niña le respondió que tenía dolor de muelas desde hacía tres días, y que su abuelo era muy pobre para llevarla al dentista en Kyoto. El monje se ofreció para ayudar y le tocó en el lugar donde le dolía. El cuarto milagro ocurrió a medida que el dolor y la hinchazón desaparecieron.

Tras 25 kilómetros de caminata, al llegar al monasterio zen, el doctor Usui se enteró de que su amigo, el anciano abad, estaba en la cama con un ataque doloroso de artritis, mal que ya lo afligía desde hacía muchos años. Mikao se fue a visitar al amigo y, mientras hablaba de sus experiencias con el monje, colocó sus manos sobre la zona afectada, y muy rápidamente desaparecieron los dolores. Le comunicó al monje que había encontrado aquello que buscaba desde hacía tantos años; le contó sobre la meditación y la visión, y le dio el nombre de Reiki a la energía que le había aplicado.

Nuevamente fue alentado por el abad y, tras alguna discusión, decidió  trabajar con su descubrimiento entre los mendigos de la ciudad de Kioto.

# El Reiki y el inicio de la divulgación

El próximo paso de Mikao Usui era poner en práctica el Reiki de la mejor forma posible. Tras unas cuantas semanas de permanencia con los monjes en el monasterio, donde el asunto fue bastante discutido, principalmente con su amigo el anciano abad, se decidió a llevar el Reiki al mundo, practicando lo que había descubierto más allá de los muros del monasterio.

Decidió que trabajaría en barrios pobres, donde las personas no tuviesen condiciones económicas para tratarse sus problemas de salud con médicos herbolarios y acupuntores. Se convirtió en vendedor ambulante de verduras en cestos, con el fin de sobrevivir y encontrar a esas personas necesitadas, y enseguida se familiarizó con los mendigos de Kioto y con todas las personas marginadas por la sociedad de su época, con el propósito de hacer que fuesen más felices, provechosas y dignas.

Su intención era curar a los mendigos y pedigüeños para que pudiesen recibir nuevos nombres en el templo y se reintegrasen de esa forma a la sociedad. Curó, primero, a los más jóvenes y habilidosos, y los mandó buscar trabajo en la ciudad para que pudiesen vivir mejor; hizo lo mismo con los más viejos y los orientó para que se ganaran la vida sin mendigar. Logró alcanzar los resultados esperados y muchos se curaron totalmente.

Cumplida esa etapa, se puso a recorrer las ciudades y aldeas repletas de indigentes y enfermos, ayudándolos con la técnica que poseía. Trabajó durante tres años junto a los alienados de la sociedad y, después de esa peregrinación por las ciudades y aldeas del Japón, regresó a Kioto donde, para su decepción y tristeza, constató que muchos de los que había ayudado e inducido a mantenerse con el trabajo honrado, habían vuelto a la mendicidad, en las mismas condiciones anteriores de miseria. Intrigado, les preguntó por qué, pudiendo trabajar, no lo hacían. Le respondieron que era más fácil mendigar que esforzarse en el trabajo.

En aquel momento comprendió que el esfuerzo realizado para beneficiar al prójimo, al que había dedicado tantos años de su vida en investigar y descubrir, y en ofrecer, parecía no ser suficiente; se dio cuenta de que había curado el cuerpo físico de los síntomas, pero no les había enseñado cómo apreciar la vida bajo un nuevo modo de vivir. Descubrió que aquellas personas no habían aprendido nada respecto a la responsabilidad, y tampoco en cuanto a la gratitud. Percibió entonces que la cura del espíritu, como la predicaban los monjes, era tan importante como la cura del cuerpo, en vista de que, con la aplicación del Reiki, sólo había validado y ratificado la condición de pedigüeños de aquellas personas. La importancia del intercambio de energía se hizo patente para él: las personas necesitaban devolver aquello que habían recibido o la vida para ellos carecería de valor.

En esa ocasión, el doctor Usui estableció los cinco principios del Reiki.

Mikao dejó el trabajo con los mendigos y resolvió enseñar a quienes deseaban conocer más; enseñaba a sus discípulos cómo curarse a sí mismos y les mostraba los principios del Reiki para ayudarles a alcanzar la armonía de los cuerpos físico, emocional, mental y espiritual.

Mikao Usui practicaba el método Reiki inspirado solamente por ideales amorosos. El Reiki, hasta entonces, consistía nada más que en el uso de la energía, los símbolos sagrados y el proceso de iniciación.

Mikao, tras su peregrinación, caminando por todo el Japón e invitando a todas las personas que sentían tristeza, depresión y dolor físico a que asistieran a sus charlas sobre Reiki, fue condecorado por el emperador del Japón, por sus curaciones y enseñanzas practicadas con ideales amorosos. Antes de fallecer, el 9 de marzo de 1926, Mikao Usui otorgó el maestrazgo del conocimiento de Reiki a dieciséis personas, mediante el mismo método tradicional milenario, el “método de boca a boca” y, entre los contemplados, se destacó el doctor Chujiro Hayashi como para ser su sucesor, entregándole la responsabilidad de transmitir y mantener intacta la tradición Reiki.

Chujiro Hayashi falleció un martes 10 de mayo de 1941, habiendo elegido antes a la señora Hawayo Takata para dar continuidad a la propagación del Reiki, en el Japón y en otras partes del mundo.

![](/assets/005)

Durante treinta años impartió cursos y curó a personas, garantizando de este modo la divulgación del Reiki en el mundo; en ese periodo sintió la necesidad de transmitir la totalidad de las enseñanzas del Reiki, y entonces, para impedir un monopolio de esa práctica, inició a veintidós maestros, recomendándoles respetar el liderazgo de su nieta Phyllis Lei Furumoto, sucesora de Takata, y dándoles permiso para formar nuevos maestros después de su muerte.

Los maestros iniciados fueron:

1. George Araki.

2. Phyllis Lei Furumoto \(nieta de Takata\).

3. Bárbara McCullough.

4. Dorothy Baba.

5. Beth Gray.

6. Mary McFadyen.

7. Úrsula Baylow.

8. John Gray.

9. Paul Mitchell.

10. Rick Bockner.

11. Iris Ishikuro.

12. Bethel Phaigh.

13. Fran Brown.

14. Harru Kuboi.

15. Bárbara Weber Ray.

16. Patricia Bowling.

17. Ethel Lombardi.

18. Shinobu Saito.

19. Wanja Twan.

20. Bárbara Brown.

21. Virginia Samdahl.

22. Kay Yamashita


El 12 de diciembre de 1980 fallece Takata, y sus cenizas son enterradas en el templo budista de Hilo. Se reunieron los veintidós maestros, y resolvieron reestructurar y dar continuidad a la “American International Reiki Association (AIRA), con sede en Florida”. Algunos maestros, debido a ciertas divergencias, crearon una segunda asociación, denominada “The Reiki Alliance”.

# Hechos y datos significativos en orden cronológico

Existen muchas controversias al respecto del material de consulta; por lo tanto, algunas fechas significativas son aproximadas:

**6000 a. de C.** Registros en el Tíbet de técnicas de curación por medio de las manos.

**620 a. de C.** Nacimiento de Siddhartha Gautama (Sakyamuni Buda) en la India.

**543 a. de C.** Muerte de Siddhartha Gautama en Kusingara (India).

**500 a. de C.** Un discípulo del Buda deja registrado a través de los Sutras, en sánscrito, los símbolos de captación de energía.

**1603 d. de C.** Japón cerró sus fronteras, prohibiendo el cristianismo, bajo pena de muerte.

**1853** Los Estados Unidos piden al Japón un puerto libre, lo que les es negado.

**1854** El japón se rinde a los Estados Unidos y a sus aliados.

**1865** Nace Mikao Usui, el 15 de agosto.

**1861/65** Comienza la guerra de secesión en Estados Unidos.

**1867** Comienza la era Meiji en Japón, con la subida al trono de Mutsu-Hito.

**1870** Los jesuítas llevan nuevamente el cristianismo al Japón.

**1878** Nacimiento de Chujiro Hayashi.

**1898** Mikao Usui viaja a Estados Unidos para estudiar.

**1898** Hawai es anexionado al territorio de los Estados Unidos.

**1900** El 24 de diciembre nace en Hawai Hawayo Takata.

**1908/09** Mikao Usui sube al monte Kurama para ayunar.

**1915** Chujiro Hayashi conoce a Mikao Usui.

**1917** El 10 de mayo se casa Hawayo con Saichi Takata.

**1925** El doctor Hayashi es iniciado como maestro de Reiki, a los 47 años de edad.

**1926** Muere Mikao Usui el 9 de marzo (deja de 16 a 18 maestros de Reiki vivos).

**1935** Los padres de Takata viajan al Japón, de vacaciones por un año.

**1935** Takata comienza su tratamiento de Reiki con el doctor Hayashi.

**1936** Hawayo Takata recibe el primer nivel de Reiki en la primavera.

**1936** En el invierno, Takata recibe el segundo nivel de Reiki.

**1936** En octubre, Takata abre su consultorio en Hawai.

**22/02/1938** Hawayo Takata es iniciada como maestra de Reiki en los Estados Unidos, durante la visita de seis meses de su maestro, Chujiro Hayashi.

**1941** El 10 de mayo muere Chujiro Hayashi (deja cinco maestros de

 Reiki vivos).

**1941/45** Guerra entre Japón y Estados Unidos.

**1970** Takata inicia la formación de sus 22 maestros.

**1970/83** Se crea la AIRA (American International Reiki Association).

**1980** El 12 de diciembre, muere Hawayo Takata (dejando 22 maestros vivos).

**1983** Nace la “Reiki Alliance”.
