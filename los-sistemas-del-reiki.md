# Los sistemas de Reiki

El objetivo de esta obra es la divulgación, principalmente, del Sistema Tradicional de Reiki Mikao Usui, o Usui Shiki Ryoho.

Existen otros sistemas o métodos de Reiki, o de utilización de la misma energía: podemos destacar el Osho Reiki, Sistema Tibetano, y el Kahuna Reiki, del que también hablaremos más adelante.

# La visión holística del cuerpo humano

Holístico es una palabra derivada del vocablo griego “holos”, que significa “todo” o “el todo”. Holístico puede ser entendido como “total” o “por entero”.

La visión holística desmonta la teoría mecanicista newtoniana que presentaba el universo compuesto de átomos, y que cada fenómeno era resultado de un proceso de “causa\/efecto”, y además que todas las relaciones físicas tenían en su base una sola causa física.

A partir de las teorías de Albert Einstein, las formas de pensamiento comenzaron a cambiar, y nació una visión moderna en Occidente que es, al mismo tiempo, muy antigua en la realidad de Oriente: el conocimiento holístico.

La física atómica presenta la materia compuesta de átomos, y éstos de partículas atómicas como protones, neutrones y electrones; posteriormente se descubrió que existían partículas subatómicas todavía menores, los “cuanta”, con formas de ondas o energía.

Einstein, en su teoría de la relatividad, con su famosa ecuación E = mc2, donde “E” equivale a energía, “m” a la masa de los cuerpos físicos y “c” a la  velocidad de la luz, nos demostró que masa y energía son una única realidad que desbancó completamente la visión Newtoniana.

# Chakras

La palabra chakra es sánscrita y significa “rueda”. En Oriente, donde los chakras se conocen desde la antigüedad, les dan nombres exóticos. Encontramos una vasta literatura al respecto de teorías orientales que son, en verdad, la base del trabajo científico de investigadores occidentales y de terapeutas. Como el Reiki trabaja, principalmente, sobre el cuerpo energético, es muy importante conocer esos trabajos.

Los chakras son centros energéticos coloridos y redondos responsables por el flujo energético en el cuerpo. Tienen como función principal absorber la energía universal, metabolizarla, alimentar nuestra aura y, finalmente, emitir energía al exterior.

En Occidente los chakras son visualizados como remolinos de energía, pequeños conos \(embudos\) de energía giratoria, que funcionan como vehículos de energía o zonas de conexión de energía, y que unen el cuerpo físico al energético, funcionando como una especie de aparato de captación y expulsión, cuyos vórtices giratorios permanecen en constante movimiento y tienen, en el ser humano normal, un diámetro de 5 a 10 centímetros.

Los chakras son responsables de innumerables acciones complejas en el cuerpo humano. A través de los chakras perdemos energía cuando estamos ante un sufrimiento físico y emocional, pues cada chakra es un punto colector de una determinada zona de conflicto y desarrollo.

Los escritos antiguos mencionan aproximadamente 88.000 chakras. Eso significa que en el cuerpo humano no existe prácticamente un punto que no sea sensible energéticamente. La mayor parte de ellos desempeña papeles secundarios.

Los chakras con los que trabaja el Reiki son los siete principales, y están localizados desde la base de la columna a la parte superior de la cabeza.

De los siete chakras principales, dos son simples; tienen apenas un vórtice (acceso): el primero y el séptimo; en cuanto a los otros, son dobles, y presentan vórtices anterior y posterior. El aura humana está asociada a esos siete chakras principales.

Los chakras se establecen en los canales energéticos; más  precisamente, en la intersección de los flujos energéticos conocidos como meridianos. Los chakras giran hacia la derecha o hacia la izquierda, y el sentido de rotación cambia de un chakra a otro, y de un sexo a otro; así, el chakra básico del hombre gira hacia la derecha, expresando un modo más activo y dominador en el ámbito material y sexual; el chakra básico de la mujer gira hacia la izquierda, expresando una mayor receptividad a la fuerza creadora de la tierra y a la fuerza en la expresión de las emociones.

![](/assets/007)

Los nombres de los chakras son de origen oriental y, en Occidente, nos referimos a ellos por los números y por el nombre de su centro físico de localización en el cuerpo humano.

