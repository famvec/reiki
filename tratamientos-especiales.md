# Tratamientos especiales

 1. **Abscesos**: Colocar una gasa o pañuelo sobre la zona, y aplicar Reiki de 15 a 30 minutos, dos veces al día.

 2. **Acné**: Cabeza 2 y 3, delante y espalda 3 y 4.

 3. **Agua**: Aplicar Reiki en el recipiente de 10 a 20 minutos.

 4. **SIDA**: Cabeza 2, 3 y 4, delante 1,2 y 3, y espalda 3 y 4.

 5. **Alcoholismo**: Cabeza 2 y 3, delante 1, 2, 3 y 4 y espalda 3.

 6. **Alergia**: Tratamiento completo, dos veces al día.

 7. **Amígdalas**: Cabeza 4, dos veces al día.

 8. **Anestesia**: Nunca aplicar Reiki en pacientes anestesiados.

 9. **Angina**: Delante 1, espalda 2 (prolongar el tiempo).

 10. **Anorexia**: Tratamiento completo.

 11. **Ansiedad**: Cabeza 1, 2, y 3, delante 2 y 3 y espalda 3.

 12. **Articulaciones**: De 15 a 30 minutos, directamente sobre la zona.

 13. **Artritis**: Tratamiento completo.

 14. **Asma**: Tratamiento completo, tiempo adicional delante 1 y 2.

 15. **Acidez**: Delante 1 y 2.

 16. **Vejiga**: Delante 4 y espalda 4.

 17. **Bronquitis**: Delante 1 y 2, espalda 1, 2, 3.

 18. **Bulimia**: Cabeza 2 y 3, delante 3 y espalda 3.

 19. **Bursitis**: Una mano en el hombro, otra en el codo, de 15 a 30 minutos, dosb veces al día.

 20. **Calambres**: Directo en el lugar, 15 minutos.

 21. **Escalofríos**: Tratamiento completo.

 22. **Cáncer**: En complemento a la quimioterapia, cabeza 3 y 4, delante 1 y 3 y

 espalda 3.

 23. **Cefalea**: Cabeza 1, 2 y 3, delante 3 y 4.

 24. **Cerebro**: Cabeza 1, 2 y 3.

 25. **Cicatriz**: Directamente en el lugar, de 15 a 20 minutos.

 26. **Celos**: Cabeza 1, 3 y 4, delante 1 y 3 y espalda 3.

 27. **Cólico**: Una de las manos sobre el estómago y la otra un poco más abajo.

 28. **Columna**: Una mano en la base, otra en la zona cervical, recorrer la columna con imposiciones secuenciales, hasta que se encuentren las dos en el centro, cinco minutos en cada punto.

 29. **Coma**: Cabeza 1, 2 y 3, delante 1, 2 y 3 y espalda 3.

 30. **Corazón**: Delante 1 y espalda 2.

 31. **Espalda**: Delante 4, espalda 4 y sobre los dolores.

 32. **Culpa, sentimiento de**: Cabeza 1 y 3 y delante 1 y 3.

 33. **Decepción/Desilusión**: Cabeza 1, 2 y 3 y delante 1 y 3.

 34. **Dientes**: Directo sobre el problema.

 35. **Depresión**: Cabeza 2 y 3, delante 1 y 3 y espalda 1, 2 y 3.

 36. **Desánimo**: Cabeza 2, 3 y 4, delante 1 y 3 y espalda 3.

 37. **Diabetes**: Tratamiento completo.

 38. **Diarrea**: Delante 4 y espalda 4.

 39. **Digestión**: Delante 2, 3 y 4 y espalda 3 y 4.

 40. **Diverticulitis**: Delante 3 y 4 y espalda 3 y 4.

 41. **Dolencias crónicas**: Tratamiento completo diariamente.

 42. **Dolores**: Directamente en la zona.

 43. **Drogas, vicios**: Cabeza 2 y 3 y delante 1, 2 y 3.

 44. **Eccema**: Cabeza 2 y 3, delante 2 y 3, y espalda 3 y sobre el lugar.

 45. **Envejecimiento precoz**: Cabeza 1,3 y 4 y delante 1.

 46. **Jaqueca**: Cabeza 1, 2 y 3 y delante 3 y 4.

 47. **Esclerosis múltiple**: Tratamiento completo.

 48. **Esquizofrenia**: Cabeza 1, 2 y 3, delante 1, 2 y 3 y espalda 3.

 49. **Fatiga**: Cabeza 1, 3 y 4, delante 1 y 3 y espalda 3.

 50. **Fiebre**: Cabeza 3 y 4 y espalda 3.

 51. **Heridas**: Directamente en el lugar, (usar como gasa).

 52. **Picaduras**: Directamente sobre la zona.

 53. **Hígado**: Delante 2 y 3, y espalda 3.

 54. **Fobias**: Cabeza 1, 2, 3 y 4, delante 1 y 3 y espalda 3.

 55. **Fracturas**: Directamente en el lugar después de enyesar.

 56. **Fumar, vicio**: Tratamiento completo.

 57. **Garganta**: Cabeza 4.

 58. **Glándulas salivares**: Cabeza 4.

 59. **Glaucoma**: Cabeza 1, 2 y 3.

 60. **Gota**: Cabeza 2 y 3, delante 2 y 3 y espalda 3.

 61. **Gravidez**: Cabeza 2 y 3, delante 1, 2, 3 y 4 y espalda 3 y 4.

 62. **Gripe, resfriado**: Tratamiento completo.

 63. **Hemorroides**: Delante 4 y espalda 4.

 64. **Hepatitis**: Tratamiento completo.

 65. **Herpes**: Directo sobre la zona afectada.

 66. **Hipertensión**: Cabeza 2, 3 y 4, delante 2 y 3 y espalda 3 y 4.

 67. **Impaciencia**: Cabeza 2 y 3 y delante 1.

 68. **Impotencia**: Cabeza 2 y 3, delante 3 y 4 y espalda 3.

 69. **Indigestión**: Delante 1, 2 y 3.

 70. **Infecciones**: Cabeza 2, delante 2 y 3, espalda 3, más imposición sobre la zona afectada.

 71. **Insomnio**: Cabeza 2 y 3.

 72. **Juanetes**: Directo sobre el lugar.

 73. **Rodillas**: Directo sobre la zona.

 74. **Laringe**: Cabeza 4.

 75. **Leucemia**: Tratamiento completo, dos veces al día.

 76. **Enfermedades de la piel**: Tratamiento completo, dos veces al día.

 77. **Tristeza**: Cabeza 4, delante 1 y 3 y espalda 3.

 78. **Mal de Alzheimer**: Cabeza 1, 2 y 3.

 79. **Malaria**: Tratamiento completo, dos veces al día.

 80. **Mandíbula, maxilar**: Directo en la zona afectada.

 81. **Memoria**: Cabeza 1, 2 y 3.

 82. **Músculos**: Directo sobre la zona.

 83. **Menopausia**: Tratamiento completo para equilibrar el sistema endocrino.

 84. **Nariz**: Directo sobre la zona.

 85. **Náusea**: Cabeza 2 y 3, delante 1 y espalda 3.

 86. **Nerviosismo**: Cabeza 2 y 3, delante 3 y espalda 3.

 87. **Nervio ciático**: Una de las manos parada en el glúteo; la otra recorrerá la pierna, desde la planta del pie hasta encontrarse con la otra, en posiciones secuenciales, con cinco minutos en cada punto; hacerlo en las dos piernas.

 88. **Neurosis**: Cabeza 1, 2 y 3, delante 3 y espalda 3.

 89. **Obesidad**: Cabeza 1, 3 y 4, delante 2 y 3, espalda 3.

 90. **Ojos**: Cabeza 1, 2 y 3.

 91. **Oídos**: Directo sobre la zona, colocar el dedo medio levemente en el canal auditivo.

 92. **Ovarios**: Delante 4.

 93. **Páncreas**: Cabeza 1, 2 y 3, delante 2 y 3 y espalda 3 y 4.

 94. **Pánico, síndrome**: Cabeza 1, 2 y 3, delante 1 y 3 y espalda 3.

 95. **Parálisis**: Tratamiento completo, dos veces al día.

 96. **Parálisis cerebral**: Tratamiento completo.

 97. **Parálisis facial**: Cabeza 4, zona del rostro, maxilar y detrás de las orejas.

 98. **Paranoia**: Cabeza 4, delante 1 y 3 y espalda 3.

 99. **Parkinson, mal de**: Tratamiento completo, dos veces al día.

 100. **Psicótico maniaco**: Cabeza 2 y 3, delante 3 y espalda 3.

 101. **Pneumonía**: Tratamiento completo dos veces al día.

 102. **Presión alta**: Cabeza 4 y delante 1.

 103. **Presión baja**: Cabeza 4 y delante 1.

 104. **Puños**: Directamente sobre la zona.

 105. **Rabia**: Cabeza 2, 3 y 4, delante 1 y 3.

 106. **Rechazo, sentimiento de**: Cabeza 2 y 3, delante 1 y 3 y espalda 3.

 107. **Resaca**: Cabeza 1, 2 y 3, delante 2 y 3, y espalda 3.

 108. **Sangramiento**: Directo sobre la zona.

 109. **Sangramiento nasal**: Pulgar en la parte inferior de la nariz, el índice en la parte superior, la otra mano en la base de la cabeza.

 110. **Senos**: Directo sobre la zona.

 111. **Síndrome de Down**: Tratamiento completo.

 112. **Sinusitis**: Directo sobre la zona, dos veces al día.

 113. **Sordera**: Cabeza 4 y sobre el oído.

 114. **Testículos**: Directo sobre la zona.

 115. **Timo**: Delante 1.

 116. **Tiroides**: Cabeza 4.

 117. **Tontera**: Cabeza 2 y 3, delante 3 y 4 y espalda 3.

 118. **Tos**: Cabeza 4, delante 1, 2 y 3 y espalda 2 y 3.

 119. **Tumores**: Cabeza 1, 2 y 3, delante 3, espalda 3 y sobre la zona afectada.

 120. **Úlcera**: Directo sobre la zona, mínimo dos veces al día.

 121. **Útero**: Delante 4 y espalda 4.

 122. **Vesícula biliar**: Delante 2 y 3.

 123. **Vicios: drogas alcohol y otros**: Cabeza 2 y 3, delante 1, 2 y 3 y espalda

 1, 2 y 3.

 124. **Vómitos**: Cabeza 3, delante 1 y 3 y espalda 3.

 125. **Voz**: Cabeza 4 y delante 1.
