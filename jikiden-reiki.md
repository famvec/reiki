# Jikiden Reiki
La gran mayoría (99%) del Reiki practicado actualmente en Japón es del estilo Occidental a pesar de que Reiki es originario del Japón. Un tiempo, se había pensado que el Reiki de Estilo Occidental era el único Reiki sobreviviente y que ya no existían sucesores del Reiki en Japón. Es así que, cuando fue reintroducido a Japón, los japoneses interesados visitaron los Estados Unidos de Norte América para aprender Reiki. Pero sí quedaban sucesores del Reiki en Japón. Ellos son:

1. Usui Reiki Ryoho Gakkai. La organización fundada por Mikao Usui, el fundador del Reiki. Esta organización fue hasta ese momento desconocida, por su decisión de permanecer como un grupo cerrado.

2. Los alumnos supervivientes de Chujiro Hayashi (un prominente alumno de Mikao Usui)

que incluyen a:

- Hawayo Takata : Una descendiente Japonesa Americana nacida en Hawai, a quien se le atribuye la expansión del Reiki hacia el Occidente. Ella se salvó milagrosamente de una grave enfermedad gracias al Reiki y se convirtió en estudiante del Maestro Hayashi. Ella entrenó a 22 maestros Reiki antes de fallecer.

- Chiyoko Yamaguchi: Quien aprendió Reiki a fines de los años 1930 cuando ella tenía 17 años de edad. Ella lo practicó diariamente por 65 años hasta su fallecimiento en el año 2003.

Su existencia fue algo muy preciado, por lo que muchas personas de la comunidad Reiki vinieron a ella para que les enseñara lo que había aprendido del Maestro Hayashi. Ella comenzó a enseñar junto con su hijo Tadao Yamaguchi en el año 1999 con el nombre de Jikiden Reiki (直傳靈氣). “Jikiden” quiere decir “directamente transmitido (del Maestro Chujiro Hayashi)”. Hoy, Tadao Yamaguchi está continuando la labor iniciada por su madre.