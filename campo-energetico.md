# Campo energético

La ciencia moderna nos dice que el organismo humano no es una mera estructura física formada por moléculas, sino que también las personas, como todo lo demás, estamos constituidas por campos energéticos.

Los científicos están aprendiendo a medir estos sutiles cambios; desarrollan instrumentos para detectar los campos energéticos relacionados con nuestros cuerpos y evaluar sus frecuencias. Miden las corrientes eléctricas del corazón con electrocardiogramas \(ECG\), y las del cerebro con encefalogramas \(EEG\). El detector de mentiras permite medir el potencial eléctrico de la piel y es posible hacer lo propio con los campos electromagnéticos que rodean el cuerpo gracias a un sensible aparato denominado SQUID \(dispositivo de interferencia del cuanto superconductor\), que ni siquiera entra en contacto con el cuerpo al medir los campos magnéticos que lo rodean.

En 1939 los doctores H. Burr y F. Northrop, de la Universidad Yale, descubrieron que midiendo el campo energético de una semilla \(lo que denominaron el L, o campo de vida\) podían determinar cuál sería el crecimiento, en términos de salud, de la planta que germinara de dicha semilla. Comprobaron que midiendo el campo de los huevos de rana podían discernir el emplazamiento futuro del sistema nervioso del batracio. Otra de sus mediciones estableció el tiempo de ovulación de la mujer, lo que les permitió sugerir un nuevo método de control de natalidad.

En 1959, el doctor Leonard Ravitz, de la Universidad William and Mary, demostró que el campo energético humano fluctúa según la estabilidad mental y psicológica de la persona.

En 1979, otro científico, el doctor Robert Becker, de la Upstate Medical School de Syracuse, Nueva York, trazó un complejo campo eléctrico sobre el cuerpo cuya forma es similar a la de éste y a la del sistema nervioso central. Lo denominó Sistema de Control de Corriente Continua y descubrió que cambiaba de forma y potencia con las mutaciones fisiológicas y psicológicas. También descubrió unas partículas del tamaño de electrones que se movían por este campo.

A medida que se ha desarrollado este conocimiento y la física newtoniana ha cedido su puesto a las teorías de la relatividad, la electromagnética y las partículas, cada vez somos más capaces de comprender la relación existente entre las descripciones objetivas científicas de nuestro mundo y el otro, el de la experiencia humana subjetiva.

La tradición espiritual india, que cuenta con más de cincuenta siglos de antigüedad, habla de una energía universal denominada Prana, considerada el constituyente básico y la fuente de toda vida. El Prana o hálito vital fluye por todas las formas, a las que ha dado vida.

En el tercer milenio a.C. los chinos propugnaban la existencia de una energía vital a la que denominaban Ch'i: toda materia, animada o no, está compuesta y trasfundida por esta energía universal. El Ch'i contiene dos fuerzas polares, el yin y el yang. Cuando están equilibradas, el sistema vital muestra salud física; si se desequilibran, el resultado es la enfermedad.

La Cábala, teosofía mística judía surgida alrededor del año 538 a.C., denomina a esta misma energía luz astral.

En la iconografía religiosa cristiana, Jesús y otras figuras espirituales aparecen rodeados por campos luminosos. El Antiguo Testamento contiene numerosas referencias a la luz que rodeaba a la gente y a la aparición de luces, pero estos fenómenos perdieron su significado original con el transcurso de los siglos. Por ejemplo, el Moisés de Miguel Ángel muestra el karnaeem en forma de dos cuernos, en vez de los dos rayos de luz a los que se refería originalmente dicho término. La razón es que, en hebreo, dicha palabra significa indistintamente cuerno o luz.

Muchos pensadores científicos occidentales han sostenido, a lo largo de la historia, la idea de una energía universal que penetra en la naturaleza de forma global. Esta energía vital percibida como un cuerpo luminoso fue registrada por los pitagóricos, por primera vez en la literatura occidental, alrededor del año 500 a.C

Boirac y Liebeault, eruditos de principios de siglo XII, vieron que la energía que poseemos los seres humanos puede dar lugar a la interacción de individuos separados por grandes distancias.

Paracelso, sabio de la Edad Media, llamó a esta energía «llliaster» entidad compuesta por una fuerza vital y una materia vital. El matemático Van Helmont percibió a comienzos del siglo XIX un fluido universal que penetra toda la naturaleza; no se trata de una materia corpórea o condensable, sino de un espíritu vital puro que invade todos los cuerpos. Otro matemático, Leibnitz, escribió que los elementos esenciales del universo son centros de fuerza que contienen su propia fuente de movimiento.

A mediados del siglo XIX, el conde Wilhelm Von Reichenbach dedicó treinta años a experimentar con el «campo» al que denominó fuerza «ódica». Comprobó que mostraba muchas propiedades similares a las del campo electromagnético descrito anteriormente, en el mismo siglo, por James Clerk Maxwell.

En 1911 el doctor William Kilner dio cuenta de sus estudios sobre el campo energético humano contemplado a través de pantallas y filtros coloreados. Describió una neblina brillante dispuesta en tres zonas alrededor de todo el cuerpo: a\) una capa oscura de unos 60 mm pegada a la piel, rodeada por b\) otra capa más vaporosa de unos 2,5 cm que fluía desde el cuerpo en sentido perpendicular, y c\) una delicada luminosidad externa de contornos indefinidos, algo más separada, con una anchura de alrededor de 15 cm. Kilner comprobó que el aspecto del «aura», como la denominó, difiere considerablemente de un sujeto a otro, dependiendo de la edad, el sexo, la capacidad mental y el estado de salud. Determinadas enfermedades producían manchas o irregularidades en el aura, lo que movió a Kilner a desarrollar un sistema de diagnóstico basado en el color, la textura, el volumen y el aspecto general del envoltorio.

A mediados del siglo XX, los doctores George De La Warr y Ruth Drown construyeron nuevos instrumentos para detectar las radiaciones de los tejidos vivos. El primero desarrolló la radiónica, un sistema de detección, diagnóstico y curación a distancia que se servía del campo energético biológico humano.

El doctor Wilhelm Reich, psiquiatra y colega de Freud a principios del siglo XX, se interesó vivamente por una energía universal a la que denominó «orgónica». Estudió las relaciones entre las alteraciones en el flujo de orgones del cuerpo humano y las enfermedades físicas y psicológicas. Reich desarrolló una modalidad psicoterapéutica que integraba las técnicas analíticas freudianas para desvelar el inconsciente con técnicas físicas destinadas a desbloquear el flujo natural de la energía orgónica en el cuerpo. Al liberar estos bloques energéticos, Reich pudo aclarar los estados mental y emocional negativos.

El doctor Lawrence Bendit y Phoebe Bendit realizaron en los años treinta amplias observaciones del CEH y relacionaron estos campos con la salud, la curación y el desarrollo del alma. El trabajo que realizaron subraya la importancia de conocer y entender las poderosas fuerzas formativas etéreas que son la base de la salud y la curación del cuerpo.

En Japón, Hiroshi Motoyama ha logrado medir niveles bajos de luz producidos por personas que han practicado yoga durante muchos años. Realizó este trabajo en un cuarto oscuro con ayuda de una cámara cinematográfica de bajo nivel lumínico.

El doctor Zheng Rongliang, de la universidad de Lanzhou \(República Popular China\), midió la energía \(denominada «Qi» o «Ch'i»\) irradiada del cuerpo humano mediante un detector biológico formado por la nerviación de una hoja conectado a un dispositivo de fotocuanto \(aparato para medir la luz de baja intensidad\).

En el Instituto Nuclear Atómico de Academia Sínica, en Shanghai, se demostró que parte de las emanaciones de fuerza vital de los maestros de qigong parece tener una onda sónica de muy baja frecuencia que se presenta como una onda portadora que fluctúa a baja frecuencia. En algunos casos, también se detectó la energía qi como flujo de micropartículas, con un tamaño de unas 60 micras de diámetro y una velocidad de unos 20-50 cm\/seg.

Hace algunos años, un grupo de científicos soviéticos del Instituto de Bioinformación de A.S. Popow anunció el descubrimiento de que los organismos vivos emiten vibraciones de energía a una frecuencia de entre 300 y 2.000 nanómetros. Los científicos soviéticos denominaron a dicha energía biocampo o bioplasma. Descubrieron que las personas capaces de realizar con éxito la transferencia de bioenergía poseían un biocampo mucho más ancho y fuerte. Estos hallazgos han sido confirmadas por la Academia de Ciencias Médicas de Moscú y están refrendados por las investigaciones realizadas en Inglaterra, los Países Bajos, Alemania y Polonia.

El estudio más extraordinario del aura humana que he visto fue realizado por la doctora Valorie Hunt y sus colaboradores en UCLA. En un estudio acerca de los efectos del rolf sobre el cuerpo y la psique \(«A Study of Structural Neuromuscular Energy Field and Emotional Approaches»\) la doctora registró la frecuencia de las señales de bajo milivoltaje emitidas por el cuerpo durante una serie de sesiones de rolf.

Las formas y frecuencias de onda consistentes se relacionaban específicamente con los colores. Dicho de otro modo, cuando se observaba un color azul en el aura en un lugar específico, las mediciones electrónicas mostraban siempre la forma y la frecuencia características de la onda azul en el mismo lugar.

En febrero de 1988, los resultados del estudio en curso mostraban las siguientes correlaciones de color\/ frecuencia \(Hz = Hertz, o ciclos por segundo\):


* Azul 250-275 Hz más 1200 Hz

* Verde 250-475 Hz

* Amarillo 500-700 Hz

* Naranja 950-1050 Hz

* Rojo 999-1250 Hz

* Violeta 1000-2000, más 300-400 Hz; 600-800 Hz

* Blanco 1100-2000 Hz

Estas bandas de frecuencia, excepto por lo que se refiere a las bandas adicionales de azul y violeta, se encuentran en orden inverso a la secuencia de color del arco iris. Las frecuencias medidas constituyen una señal identificativa de la instrumentación y de la energía que se mide.

La doctora Hunt añadió que «los chakras solían tener los colores que se habían consignado en la literatura metafísica, es decir, rojo-kundalini, naranja-hipogástrico, amarillo-bazo, verde-corazón, azul-garganta, violeta-tercer ojo y blanco-corona.

Si definimos el campo energético humano como todos los campos o emanaciones del cuerpo del individuo, podremos ver que muchos componentes bien conocidos del CEH han sido medidos en laboratorio. Son los componentes electrostáticos, magnéticos, electrónicos, sónicos, térmicos y visuales del CEH. Todas estas mediciones concuerdan con los procesos fisiológicos normales del cuerpo, superándolos para aportar un vehículo al funcionamiento psicosomático.

