# Meridianos y nadis

Según la cultura china, los meridianos son los canales energéticos que recorren el cuerpo humano y conducen la energía vital. Cada uno de los meridianos está relacionado con un órgano y con una función, los cuales, a su vez, están relacionados con el principio chino del Yin y el Yang. Cuando la energía que los recorre se halla desequilibrada es posible reequilibrarlos estimulando los meridianos en diversos puntos, y ese es el principio de la acupuntura, del “Do-in” y del Shiatsu.

![](/assets/008)

Los nadis son tres: Sushumna, Ida y Pingala. Ida y Pingala tienen la capacidad de captar el prana directamente del aire, a través de la respiración, y expedir tóxicos durante la exhalación. De ello se deduce la importancia de una buena práctica respiratoria.

Ida es el canal conductor de energía lunar (tranquilizadora). Ese canal básico comienza en el lado izquierdo del chakra básico y termina en la parte superior izquierda de la nariz.

Pingala es el canal conductor de energía solar (estimulante). Ese canal comienza en el lado derecho del chakra básico y termina en la parte superior derecha de la nariz.

Sushumna es el canal por medio del cual se procesa el descenso y la subida de energía cósmica. Todos los chakras tienen sus “raíces” en ese canal, que va desde el chakra coronario al chakra básico.
