# Cómo proceder con su aplicación a otras personas

El color de la ropa del terapeuta no importa, debe ser cómoda y limpia, para evitar las impregnaciones energéticas negativas en ropas muy usadas. La energía Reiki pasa sin problemas a través de la ropa y otros materiales, por eso no es necesario que el paciente se la quite.

Deben retirarse todos los adornos de metal, tanto del receptor como del practicante, argollas, anillos, pulseras, cadenas, cristales y reloj; éstos poseen vibraciones propias que pueden interferir en la energía Reiki. Una medalla, un anillo o una pulsera que no se logra, o no se quiere quitar por cuestiones religiosas, no causarán una gran interferencia.

Si lo desea, el terapeuta puede decir una oración silenciosa en agradecimiento por la gracia de poder actuar con la energía divina y funcionar como puente y canal de la energía universal.

Usted es un canal de Reiki; lo que cura es la energía de Dios, por eso acuérdese que el honor es de él.

Evite hacer diagnósticos: eso es prerrogativa médica, y no interfiera en el tratamiento a que la otra persona esté siendo sometida; recuerde que el Reiki es una terapia alternativa y también complementaria. En consecuencia, utilícelo también como complemento.

La cantidad y la calidad del Reiki en la aplicación está determinada por quien lo recibe; por esto, actúe con cautela contra la creación de patrones de reacción y estímulos. En ocasiones, las personas pueden sentir calor, hormigueo, palpitaciones, vibraciones; muchas veces pueden no sentir nada, o apenas un leve relajamiento; sentir o no sentir no es un parámetro para juzgar la eficiencia del tratamiento.

Al iniciar cualquier tratamiento, especialmente en enfermedades graves, es recomendable hacer tres aplicaciones seguidas, durante tres días consecutivos, facilitando así una respuesta más rápida del sistema inmunológico, pero eso no es una regla. El terapeuta y la otra persona deben establecer, en forma conjunta, el cronograma de aplicaciones, de acuerdo con la disponibilidad de tiempo de ambos.

Procure averiguar los motivos de la situación que llevó a la persona a estar dispuesta a recibir el Reiki; tal procedimiento ayuda y facilita un derrotero seguro para la aplicación, y podremos escoger las posiciones más específicas para el caso.

Tanto el paciente como el terapeuta deberán mantener piernas, dedos y brazos sin cruzarlos, para que todos los canales de energía del cuerpo puedan recibir la misma cantidad y se mantengan igualmente desbloqueados y limpios.

Durante toda la sesión, el practicante debe buscar una posición cómoda, apoyar la espalda, relajar los hombros y, si fuese posible, apoyar los brazos.

El tiempo de aplicación en cada posición en el nivel I es de cinco minutos, mientras que el tratamiento completo lleva setenta minutos, pues son catorce posiciones básicas, pero no existe ningún impedimento para dedicar un tiempo menor de aplicación, e incluso de no hacer todas las posiciones. Para escoger algunas, siga su intuición. Sensibilícese al campo de energía de la otra persona y sentirá lo que es más adecuado a cada situación. El ideal es el tratamiento completo; sin embargo, si se hace imposible, poco Reiki es mejor que ninguno.

En cuanto a las manos, el reikiano mantendrá los dedos unidos y la mano en forma de concha, ligeramente curvados, para que no haya dispersión de energía, como si estuviésemos bebiendo agua, aunque sin tensionarlos, dejándolos flexibles y suaves.

El tratamiento deberá comenzar por la cabeza, siguiendo todas las posiciones convencionales, con las manos colocadas suavemente; los cambios de posición, siempre que sean posibles, se hacen moviendo apenas una mano cada vez, para no interrumpir el contacto. Para aplicar energía sobre la región genital, es conveniente utilizar una toalla o manta doblada sobre esa parte del cuerpo, para evitar malas interpretaciones. Otra forma es colocar las manos suspendidas, a dos o tres centímetros del cuerpo, o pedir que el receptor coloque sus manos sobre la zona; entonces, aplicaremos la energía a través de las manos del paciente.

Después del tratamiento es aconsejable, quedando a criterio del terapeuta, agradecer a Dios, en una oración silenciosa, por la oportunidad de haber podido trabajar para alguien como canal Divino.

Con el fin de romper la interacción de campos áuricos, el terapeuta deberá lavarse las manos con agua corriente, luego de haber terminado la sesión. O frotarse las manos vigorosamente y soplar hacia ellas, para cortar el contacto con la persona tratada, haciendo que el flujo del Reiki cese.

El pago de terapias es lícito, necesario y correcto, dentro del pleno respeto de la ley de la naturaleza que requiere el libre intercambio de energía. El reikista cobra por el tiempo dedicado como canal en el tratamiento, y no por la energía Reiki, que es divina, regalada e ilimitada. El Reiki también puede ser aplicado en animales, en semillas y en plantas.

.

![](/assets/011)

