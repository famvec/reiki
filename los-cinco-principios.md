# Los cinco principios del Reiki

Éstos fueron los principios dejados por el doctor Mikao Usui para que fueran  transmitidos a lo largo del tiempo:

Principios del doctor Mikao Usui

1. Solo por hoy, no sienta rabia ni se ponga de mal humor.
2. Solo por hoy, abandone sus preocupaciones.
3. Solo por hoy, agradezca sus bendiciones, respete a sus padres, maestros y a los más ancianos.
4. Solo por hoy, haga su trabajo honradamente.
5. Solo por hoy, muestre amor y respeto y sea gentil con todos los seres vivos.
