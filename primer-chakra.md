# Primer chakra o chakra básico

El chakra básico se sitúa en la base de la columna vertebral, entre el ano y los organos sexuales, en la cintura pélvica.

Este chakra está abierto hacia abajo y representa la unión del hombre con la tierra o con el mundo material y físico, y está vinculado con nuestra existencia terrena, con nuestra supervivencia. Se relaciona con el nivel de la conciencia que nos permite sobrevivir en el mundo, con todo lo material, sólido y corporal, así como también con nuestra energía física y con nuestros deseos de vivir en el mundo físico.

Cuanto más abierto y vitalizado se encuentre este chakra, más elevada será nuestra energía física \(disposición\). Y así estaremos bien enraizados, y viviremos con determinación y constancia en nuestras vidas. Por eso están concentradas en él las cualidades que tienen que ver con la tierra y los medios de supervivencia, como, por ejemplo: el alimento, el aire, el agua, los recursos económicos, el trabajo o el empleo, la capacidad de lucha, ganar y gustar del dinero, luchar por la realización de sus ideales y deseos, tener rumbo y orientación y no depender de otras personas; es decir, todo los que es necesario para nuestra existencia.

Si el reikista siente que el primer chakra necesita mucha energía, puede  diagnosticar fácilmente que el paciente tiene dificultades en una o en todas las  cualidades indicadas anteriormente.

El color de este chakra es rojo o negro; así, usando estos colores, su energización puede ser acelerada. Si cuando está activo tienen color rojo fuego, su elemento correspondiente es la “tierra”, y su sonido correspondiente es el Lam.

Su centro físico corresponde a las glándulas suprarrenales, que producen la adrenalina, y que tienen la función de regular la circulación y equilibrar la temperatura del cuerpo, preparándolo para reacciones inmediatas.

El desequilibrio del chakra básico produce físicamente anemia \(deficiencia  de hierro\), leucemia, problemas de circulación, presión baja, poca tonicidad  muscular, fatiga, insuficiencia renal, exceso de peso.

Los bloqueos en el chakra básico frecuentemente desembocan en  síntomas y actitudes mentales, como pacifismo extremo \(“¡yo no consigo hacer mal ni a una cucaracha!”\), miedo existencial \(“¡nadie con conciencia sana podría tener hijos actualmente!”\), agresión excesiva \(“vamos a agredir a este loco nauseabundo!”\), miedo a la muerte \(“¡no quiero correr ningún riesgo!”\), problemas con el planeamiento del tiempo \(“¡no sé por qué estoy siempre atrasado!”\), impaciencia \(“¿por qué ese idiota no se quita del medio?”\) y dependencia \(“¡no consigo vivir sin él\/ella!”\). Es el chakra que capta la energía para mantener en nuestro cuerpo la columna vertebral, los riñones, los huesos, los dientes, el intestino grueso, el ano y el recto.

### Chakra Básico - 1er. Chakra
| Concepto | Valor |
| -- | -- |
| Nombre | Muladhara |
|  Localización | Base de la columna vertebral |
| Color  | Rojo |
| Cuerpo áurico | Tierra |
| Elemento | Tierra |
| Nota musical | Do |
| Mantra | Lam |
| Número de pétalos | 4 |