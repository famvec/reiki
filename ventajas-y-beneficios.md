# Ventajas y beneficios
El Reiki se encuentra al alcance de todos, inclusive de los niños, ancianos y enfermos. Todos podemos ser un canal de Reiki; no existe límite de edad, ni exige ninguna condición previa.

La técnica es segura, sin efectos secundarios ni contraindicaciones, siendo compatible con cualquier otro tipo de terapia o tratamiento.

No es un sistema religioso o filosófico que proponga restricciones ni tabúes.

No utiliza talismanes, rezos, mentalizaciones, visualizaciones, fe, ni ningún objeto, para su aplicación práctica.

Tras la sintonización energética que ocurre durante el seminario, usted podrá aplicar Reiki, inmediatamente, durante el resto de su vida, a pesar de que deje de practicarlo durante un largo periodo; y no existe la necesidad de una nueva activación para el mismo nivel.

La energía no está polarizada, no tiene positivo ni negativo (yin y yang).

El Reiki es semejante a una onda de radio, y puede aplicarse adecuadamente en el mismo lugar o a distancia.

Está por encima del tiempo y el espacio, permitiendo de esta forma reprogramar acontecimientos pasados y coordinar acontecimientos futuros.

La energía no es manipulativa; el practicante coloca simplemente las manos y la energía fluye en la intensidad y en la calidad determinada por quien la recibe.

No es necesario desnudar al paciente durante la aplicación, pues la energía penetra a través de cualquier cosa.

El terapeuta no necesita conocer el diagnóstico de la patología para efectuar con éxito el tratamiento.

El Reiki energiza y no desgasta al practicante, pues la técnica no utiliza el “Chi” o “Ki” del practicante, y sí la Energía Vital del Universo.

El Reiki es un recurso óptimo para equilibrar los siete chakras principales, que están localizados desde la base de la columna a la parte superior de la cabeza.

El Reiki alivia rápidamente los dolores físicos.

Considera a la persona de forma holística. Puede utilizarse tanto en el tratamiento de uno mismo, como en el tratamiento de otras personas, plantas y animales.