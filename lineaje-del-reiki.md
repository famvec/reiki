# El linaje del Reiki

Tras la iniciación, el reikista pasa a pertenecer a un árbol o linaje de maestros, y se torna importante conocer esa cadena de cada maestro.

# PRIMER LINAJE SISTEMA JAPONÉS \(USUI\) REIKI

1. Mikao Usui

2. Chujiro Hayashi

3. Hawayo Takata

4. Phyllis Lei Furumoto

5. Pat Jack Carol Fanner

6. Cherie A. Prashn Lean Smith

7. William Lee Rand

8. Johnny De' Carlii

9. Francisco Vera Larraín

10. “Usted”


